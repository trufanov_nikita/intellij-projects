import React, { useState, useEffect, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilter } from '@fortawesome/free-solid-svg-icons';

function FilterData() {
    const [tags, setTags] = useState([]);
    const [selectedTags, setSelectedTags] = useState([]);
    const [showSortingBox, setShowSortingBox] = useState(false);
    const [iconRotation, setIconRotation] = useState(0);
    const [searchQuery, setSearchQuery] = useState('');
    const filterBoxRef = useRef(null);

    useEffect(() => {
        fetchTags();
        const handleClickOutside = (event) => {
            if (filterBoxRef.current && !filterBoxRef.current.contains(event.target)) {
                setShowSortingBox(false);
                setIconRotation((prevRotation) => prevRotation + 180);
            }
        };

        document.addEventListener('click', handleClickOutside);
        return () => {
            document.removeEventListener('click', handleClickOutside);
        };
    }, []);

    useEffect(() => {
        const storedTags = JSON.parse(localStorage.getItem('selectedTags'));
        if (storedTags) {
            setSelectedTags(storedTags);
        }
    }, []);

    const fetchTags = async () => {
        try {
            const response = await fetch('/tag');
            const data = await response.json();
            // Remove duplicates based on tag name
            const uniqueTags = Array.from(new Set(data.map((tag) => tag.name))).map((name) =>
                data.find((tag) => tag.name === name)
            );
            setTags(uniqueTags);
        } catch (error) {
            console.error('Error fetching tags:', error);
        }
    };

    const handleTagChange = (tagId, tagName) => {
        const tag = tags.find((tag) => tag.id === tagId);
        if (!tag) {
            return;
        }
        const selectedTag = { id: tag.id, name: tagName };
        setSelectedTags((prevSelectedTags) =>
            prevSelectedTags.some((t) => t.id === tagId)
                ? prevSelectedTags.filter((t) => t.id !== tagId)
                : [...prevSelectedTags, selectedTag]
        );
    };

    const handleSave = () => {
        localStorage.setItem('selectedTags', JSON.stringify(selectedTags));
        window.location.reload();
    };

    const handleReset = () => {
        setSelectedTags([]);
    };

    const handleSearch = (event) => {
        setSearchQuery(event.target.value);
    };

    const filteredTags = tags.filter((tag) =>
        tag.name.toLowerCase().includes(searchQuery.toLowerCase())
    );

    return (
        <div className='sorting-container'>
            <button
                onClick={(event) => {
                    event.stopPropagation();
                    setShowSortingBox(!showSortingBox);
                    setIconRotation((prevRotation) => prevRotation + 180);
                }}
            >
                <FontAwesomeIcon
                    icon={faFilter}
                    style={{
                        color: '#0f4db8',
                        transform: `rotate(${iconRotation}deg)`,
                        transition: `transform 0.5s ease`,
                    }}
                />
            </button>
            {showSortingBox && (
                <div className='filter-data' ref={filterBoxRef}>
                    <div className='filter-form'>
                        <div className='filter-search'>
                            <input
                                className='filter-input'
                                type='text'
                                placeholder='Search for tag'
                                value={searchQuery}
                                onChange={handleSearch}
                            />
                        </div>
                        <div className='filter-tags'>
                            {filteredTags.map((tag) => (
                                <label key={tag.id}>
                                    <input
                                        type='checkbox'
                                        checked={selectedTags.some((t) => t.id === tag.id)}
                                        onChange={() => handleTagChange(tag.id, tag.name)}
                                    />
                                    {tag.name}
                                </label>
                            ))}
                        </div>
                        <div className='filter-buttons'>
                            <button onClick={handleSave}>Apply</button>
                            <button onClick={handleReset}>Reset</button>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}

export default FilterData;
