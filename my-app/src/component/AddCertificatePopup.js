import React, { useState } from 'react';
import Button from './Button';
import { WithContext as ReactTags } from 'react-tag-input';
import CustomError from './CustomError';
import Cookies from 'universal-cookie';
const AddCertificatePopup = ({ getData, setError }) => {
  const [newCertificate, setNewCertificate] = useState({
    name: '',
    description: '',
    price: 0,
    duration: 0,
    tags: [],
  });

  const [showAddPopup, setShowAddPopup] = useState(false);

  const handleAddCertificate = () => {
    setShowAddPopup(true);
  };

  const handleCancelAddCertificate = () => {
    setShowAddPopup(false);
  };

  const handleAddition = (tag) => {
    const isTagAlreadyExists = newCertificate.tags.some((existingTag) => existingTag.name === tag.text);
    if (isTagAlreadyExists) {
      setError(new CustomError(`Tag with name ${tag.text} already exists`, 401));
      return;
    }

    setNewCertificate((prevCertificate) => ({
      ...prevCertificate,
      tags: [...prevCertificate.tags, { name: tag.text }],
    }));
  };

  const handleDelete = (tagIndex) => {
    setNewCertificate((prevCertificate) => {
      const updatedTags = [...prevCertificate.tags];
      updatedTags.splice(tagIndex, 1);
      return {
        ...prevCertificate,
        tags: updatedTags,
      };
    });
  };
  const handleNewCertificateChange = (field, value) => {
    setNewCertificate((prevCertificate) => ({
      ...prevCertificate,
      [field]: value,
    }));
  };
  const handleAddNewCertificate = async () => {
    try {
      if (newCertificate.name.length < 4) {
        setError(new CustomError('Name must have at least 4 characters', 400));
        return;
      }
      if (newCertificate.description.length < 14) {
        setError(new CustomError('Description must have at least 14 characters', 400));
        return;
      }
      if (newCertificate.price <= 0 || isNaN(newCertificate.price)) {
        setError(new CustomError('Price must be a positive number', 400));
        return;
      }
      if (newCertificate.duration <= 0 || isNaN(newCertificate.duration)) {
        setError(new CustomError('Duration must be a positive number', 400));
        return;
      }

      const cookie = new Cookies();
      const jwtToken = cookie.get('jwt');
      const response = await fetch('/certificate', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${jwtToken}`,
        },
        body: JSON.stringify(newCertificate),
      });

      if (response.status !== 201) {
        const err = await response.json();
        console.log(err);
        throw new CustomError(err.message, err.status);
      }
      setNewCertificate({
        name: '',
        description: '',
        price: '',
        duration: '',
        tags: [],
      });
      setShowAddPopup(false);
      getData();
    } catch (error) {
      setError(error);
    }
  };
  const tagsForRendering = newCertificate.tags.map((tag, index) => ({ id: index.toString(), text: tag.name }));
  return (
    <>
      <button onClick={handleAddCertificate}>Add Certificate</button>
      {showAddPopup && (
        <div className="popup">
          <div className="add-certificate-wrapper">
            <h2>Add Certificate</h2>
            <label htmlFor="name">Name:</label>
            <input
              type="text"
              id="name"
              className="input-field"
              value={newCertificate.name}
              onChange={(e) => handleNewCertificateChange('name', e.target.value)}
            />
            <label htmlFor="description">Description:</label>
            <textarea
              id="description"
              className="description-field"
              value={newCertificate.description}
              onChange={(e) => handleNewCertificateChange('description', e.target.value)}
            />
            <label htmlFor="price">Price:</label>
            <input
              type="number"
              min="0"
              id="price"
              className="input-field"
              value={newCertificate.price}
              onChange={(e) => handleNewCertificateChange('price', e.target.value)}
            />
            <label htmlFor="duration">Duration:</label>
            <input
              type="number"
              min="0"
              id="duration"
              className="input-field"
              value={newCertificate.duration}
              onChange={(e) => handleNewCertificateChange('duration', e.target.value)}
            />
            <label htmlFor="tags">Tags:</label>
            <ReactTags
              tags={tagsForRendering}
              handleDelete={(index) => handleDelete(index)}
              handleAddition={(tag) => handleAddition(tag)}
              handleDrag={() => console.log("Drag")}
            />
            <div className="popup-buttons">
              <Button text="Add" backColor="green" onClick={handleAddNewCertificate} />
              <Button text="Cancel" backColor="red" onClick={handleCancelAddCertificate} />
            </div>
          </div>
        </div>)}
    </>)
};

export default AddCertificatePopup;