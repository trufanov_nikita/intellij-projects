import Error from "./Error";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import tempImage from '../assets/temp-item-image.jpg'
function CertificateMain({ certificateNumber }) {
    const [data, setData] = useState(null);
    const [initialData, setInitialData] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    useEffect(() => {
        const controller = new AbortController();
        const getData = async () => {
            try {
                setError(null);
                setLoading(true);
                console.log(certificateNumber);
                const response = await fetch(`${certificateNumber}`);
                if (!response.ok) {
                    const errorData = await response.json();
                    throw new Error(errorData.message);
                }
                let actualData = await response.json();
                setData(new CertificateClass(actualData));
                setInitialData(actualData);
            }
            catch (err) {
                setError(err);
            }
            finally {
                setLoading(false);
            }
        }
        getData();
        return () => {
            console.log("cleared");
            controller.abort();
        }
    }, []);

    function GetTags() {
        const tags = data.tags;
        return tags.map(({ id, name, _link }) => (
            <div className='tags-array'>
                <Link key={id} to={`/tag/${id}`}> {name} </Link>
            </div>
        ))
    }

    return (
        <div className='item-page-wrapper'>
            <div className="item-detail-left">
                <img src={tempImage} alt="item-image" className="item-image"></img>
                <div className="item-detail-text">
                    <h2>Item Detailed Description</h2>
                    <div className="description">
                        {data && (<span className='description-text'>{data.description}</span>)}
                    </div>
                </div>
            </div>
            <div className="item-detail-right">
                <h1 className="coupon-name">Tags</h1>
                <div className="coupon-top">
                    <div className="item-tags">{data && GetTags()}
                    </div>
                </div>
                <div className="coupon-bottom">
                    <div className="time-to-buy">
                        <span className="time-to-buy-text">
                            Duration
                        </span>
                        <span className="time-to-buy-date">
                            {data && data.duration}
                        </span>
                    </div>
                    <div className="item-add-to-cart">
                        <h2 className="price">${data && data.price}</h2>
                        <div className="add-to-cart">
                            <button type="button" className="add-to-cart-button">
                                Add to Cart
                            </button>
                            <img src="../assets/busket.svg" alt="" className="cart-image"></img>
                        </div>
                    </div>
                </div>
            </div>
            {loading && <div>A moment please...</div>}
            {error && <Error error={error} /> }
        </div>
    )
}

class CertificateClass {
    constructor(data) {
        this.createdBy = data.createdBy;
        this.createdDate = data.createdDate;
        this.lastModifiedBy = data.lastModifiedBy;
        this.lastModifiedDate = data.lastModifiedDate;
        this.id = data.id;
        this.name = data.name;
        this.description = data.description;
        this.price = data.price;
        this.duration = data.duration;
        this.createDate = data.createDate;
        this.lastUpdateDate = data.lastUpdateDate;
        this.tags = data.tags;
        this._links = data._links;
    }
}

export default CertificateMain;