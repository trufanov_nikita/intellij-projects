import React, { useState, useEffect } from 'react';
import Cookies from 'universal-cookie';
import Pagination from './Pagination';
import { useNavigate, useSearchParams } from 'react-router-dom';
import Error from './Error';
import CustomError from './CustomError';
import TableData from './TableData';
import AddCertificatePopup from './AddCertificatePopup';
import DeleteConfirmationPopup from './DeleteConfirmationPopup';

const PAGE_SIZE = 10;
const SORT_PROPERTY = "createdDate";
const SORT_DIRECTION = "ASC";
function Main({ page }) {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [showPopup, setShowPopup] = useState(false);
  const [certificateToDelete, setCertificateToDelete] = useState(null);
  const [expandedTags, setExpandedTags] = useState({});
  if (!page) {
    page = 1;
  }
  const [currentPage, setCurrentPage] = useState(page);
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const getData = async () => {
    try {
      setLoading(true);
      let response;
      const selectedTags = JSON.parse(localStorage.getItem('selectedTags'));
      if (selectedTags && selectedTags.length > 0) {
        const tagsQueryString = `&` + selectedTags.map((tag) => `tags=${tag.name}`).join('&');
        response = await fetch(
          `/certificate/pagination/tag?page=${currentPage - 1}
          &pageSize=${localStorage.getItem('pageSize') || PAGE_SIZE}
          &sortProperty=${localStorage.getItem('sortProperty') || SORT_PROPERTY}
          &sortDirection=${localStorage.getItem('sortDirection') || SORT_DIRECTION}
          ${tagsQueryString}`
        );
      }
      else {
        response = await fetch(
          `/certificate/pagination/modified?page=${currentPage - 1}
          &pageSize=${localStorage.getItem('pageSize') || PAGE_SIZE}
          &sortProperty=${localStorage.getItem('sortProperty') || SORT_PROPERTY}
          &sortDirection=${localStorage.getItem('sortDirection') || SORT_DIRECTION}`
        );
      }
      if (response.status === 500) {
        throw new CustomError("Backend is not available", response.status)
      }
      if (response.status !== 200) {
        console.log(response);
        let error = await response.json();
        console.log(error);
        throw new CustomError(response.statusText, response.status)
      }
      let actualData = await response.json();
      if (!actualData._embedded) {
        setError(new CustomError("There is no data in this page", 404))
      }
      else {
        setData(actualData);
        setError(null);
      }
    } catch (err) {
      setError(err);
      console.log(err)
      setData(null);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    const controller = new AbortController();
    getData();
    return () => {
      controller.abort();
    }
  }, [currentPage]);

  useEffect(() => {
    console.log("page");
    setCurrentPage(page);
    setSearchParams({ page });
  }, [page, setSearchParams]);

  const changePage = (page) => {
    navigate(`/certificate/?page=${page}`);
  };

  const deleteCertificate = async (certificateId) => {
    try {
      const cookie = new Cookies();
      const jwtToken = cookie.get('jwt');
      const response = await fetch(`/certificate/${certificateId}`, {
        method: "DELETE",
        headers: {
          "Authorization": `Bearer ${jwtToken}`,
        },
      });

      if (response.status !== 200) {
        throw response;
      }

      getData();
    } catch (error) {
      setError(error);
    }
  };

  const handleDeleteConfirmed = () => {
    if (certificateToDelete) {
      deleteCertificate(certificateToDelete.id);
      setShowPopup(false);
    }
  };

  const handleDeleteCancelled = () => {
    setShowPopup(false);
  };

  return (
    <div className='main'>
      {loading && <div>A moment please...</div>}
      {error && (
        <Error error={error} />
      )}
      <AddCertificatePopup getData={getData} setError={setError} />
      {data && (
        <>
          <TableData
            data={data}
            setCertificateToDelete={setCertificateToDelete}
            setShowPopup={setShowPopup}
            expandedTags={expandedTags}
            setError={setError}
            setExpandedTags={setExpandedTags}
            getData={getData}
          />
          <Pagination
            className="pagination-bar"
            currentPage={currentPage}
            totalPageCount={GetPageParam(data._links.lastPage.href)}
            onPageChange={page => changePage(page)}
          />
        </>
      )}
      {showPopup && certificateToDelete && (
        <DeleteConfirmationPopup
          certificateName={certificateToDelete.name}
          onDelete={handleDeleteConfirmed}
          onCancel={handleDeleteCancelled}
        />
      )}
    </div>
  );
}

function GetPageParam(url) {
  const params = new URL(url).searchParams;
  const page = params.get("page");
  return parseInt(page);
}

export default Main
