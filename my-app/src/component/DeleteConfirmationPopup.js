import React from 'react';
import Button from './Button';

const DeleteConfirmationPopup = ({ certificateName, onDelete, onCancel }) => {
  return (
    <div className="popup">
      <div className="popup-content">
        <h3>{`Do you want to delete certificate with name: ${certificateName}?`}</h3>
        <div className="popup-buttons">
          <Button text="Yes" backColor="red" onClick={onDelete} />
          <Button text="No" backColor="green" onClick={onCancel} />
        </div>
      </div>
    </div>
  );
};

export default DeleteConfirmationPopup;