import React from 'react';

const CustomScrollbar = ({ totalPages, currentPage }) => {
  let scrollThumbHeight = '100%';
  if (totalPages > 1) {
    // Calculate the height of the scrollbar
    const percentage = (currentPage / (totalPages - 1)) * 100;
    scrollThumbHeight = `${percentage}%`;
  }
  return (
    <div className='custom-scrollbar-container'>
      <div className='custom-scrollbar'>
        <div
          className='custom-scrollbar-thumb'
          style={{ height: scrollThumbHeight }}
        ></div>
      </div>
    </div>
  );
};

export default CustomScrollbar;
