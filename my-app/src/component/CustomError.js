class CustomError {
  constructor(message, status) {
    this.message = message
    this.status = status
  }
}
export default CustomError;