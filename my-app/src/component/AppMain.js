import React from 'react'
import Button from './Button'
import { useNavigate } from 'react-router-dom';
function AppMain() {
  const navigate = useNavigate();
  return (
    <div className='main'>
      <div className='index-page-wrapper'>
        <h1>Certificate service</h1>
        <Button text={'Certificates'} onClick={() => navigate('/certificate?page=0')} backColor={'#2196f3'} />
        <div className='index-buttons'>
          <Button text={'Login'} onClick={() => navigate('/login')} backColor={'	#b52b79'} />
          <Button text={'Sign Up'} onClick={() => navigate('/register')} backColor={'#7464bc'} />
        </div>
      </div>
    </div>
  )
}

export default AppMain
