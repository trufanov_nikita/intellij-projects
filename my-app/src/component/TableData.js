import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons';
import formatDateTime from './FormateDateTime';
import Button from './Button';
import { WithContext as ReactTags } from 'react-tag-input';
import Cookies from 'universal-cookie';
import CustomError from './CustomError';
import { useNavigate } from 'react-router-dom';

function TableData({ data, setCertificateToDelete, setShowPopup, expandedTags, setError, setExpandedTags, getData }) {

  const navigate = useNavigate();
  const [tagError] = useState('');
  function GetLinkToCertificate(href) {
    const number = extractNumberFromLink(href);
    if (number) {
      return (`/certificate/${number}`);
    }
  }

  function extractNumberFromLink(apiLink) {
    const parts = apiLink.split('/');
    const lastPart = parts[parts.length - 1];
    const number = parseInt(lastPart, 10);
    return isNaN(number) ? null : number;
  }

  function GetLinkToTag(href) {
    const number = extractNumberFromLink(href);
    if (number) {
      return (`/tag/${number}`);
    }
  }

  const [editingId, setEditingId] = useState(null);
  const [editedData, setEditedData] = useState({});

  const view = (certificateId) => {
    navigate(`/certificate/${certificateId}`);
  };

  const handleDeleteCertificate = (certificateId, certificateName) => {
    setCertificateToDelete({ id: certificateId, name: certificateName });
    setShowPopup(true);
  };

  const edit = (certificateId) => {

    if (editingId !== null) {
      setEditingId(null);
      setEditedData({});
    }
    setEditingId(certificateId);
    const rowData = data._embedded.giftCertificateList.find(
      (item) => item.id === certificateId
    );
    setEditedData(rowData);
  };

  const update = async (certificateId) => {
    try {
      if (editedData.name.length < 4) {
        setError(new CustomError('Name must have at least 4 characters', 400));
        return;
      }
      if (editedData.description.length < 14) {
        setError(new CustomError('Description must have at least 14 characters', 400));
        return;
      }
      if (editedData.price <= 0 || isNaN(editedData.price)) {
        setError(new CustomError('Price must be a positive number', 400));
        return;
      }
      if (editedData.duration <= 0 || isNaN(editedData.duration)) {
        setError(new CustomError('Duration must be a positive number', 400));
        return;
      }
      const cookie = new Cookies();

      const updatedCertificate = {
        ...editedData,
      };

      const response = await fetch(`/certificate/${certificateId}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${cookie.get('jwt')}`,
        },
        body: JSON.stringify(updatedCertificate),
      });

      if (response.status !== 200) {
        throw response;

      }
      setEditingId(null);
      setEditedData({});
      getData();
    } catch (error) {
      setEditingId(null);
      setEditedData({});
      setError(error);
    }
  };

  const cancel = () => {
    setEditingId(null);
    setEditedData({});
  };

  const handleTagAddition = (tag) => {
    const isTagAlreadyExists = editedData.tags.some((existingTag) => existingTag.name === tag.text);
    if (isTagAlreadyExists) {
      setError(new CustomError(`Tag with name ${tag.text} is already exists `, 401));
      return;
    }

    setEditedData({
      ...editedData,
      tags: [...editedData.tags, { name: tag.text }],
    });
  };

  const handleDeleteTag = (tagIndex) => {
    const updatedTags = [...editedData.tags];
    updatedTags.splice(tagIndex, 1);
    setEditedData({
      ...editedData,
      tags: updatedTags,
    });
  };

  const table = data._embedded.giftCertificateList.map(
    ({ id, name, description, price, duration, createdDate, lastModifiedDate, tags, _links }) => {
      const isEditing = editingId === id;
      const isTagsExpanded = expandedTags[id];
      return (
        <tr key={id}>
          <td>{id}</td>
          <td>{isEditing ? <input value={editedData.name || ""} onChange={(e) => setEditedData({ ...editedData, name: e.target.value })} /> : <Link to={GetLinkToCertificate(_links.self.href)}>{name}</Link>}</td>
          <td>{isEditing ? <textarea className='description-table-input' value={editedData.description || ""} onChange={(e) => setEditedData({ ...editedData, description: e.target.value })} /> : <span className='descripion-table'>{description}</span>}</td>
          <td>{isEditing ? <input type='number' min="0" value={editedData.price || ""} onChange={(e) => setEditedData({ ...editedData, price: e.target.value })} /> : `${price}$`}</td>
          <td>{isEditing ? <input type='number' min="0" value={editedData.duration || ""} onChange={(e) => setEditedData({ ...editedData, duration: e.target.value })} /> : duration}</td>
          <td>{formatDateTime(createdDate)}</td>
          <td>{formatDateTime(lastModifiedDate)}</td>
          <td >
            <div className={isEditing ? '' : 'tag-row'}>
              {isEditing ? (
                <ReactTags
                  tags={editedData.tags.map((tag, index) => ({ id: index.toString(), text: tag.name }))}
                  handleDelete={(index) => handleDeleteTag(index)}
                  handleAddition={(tag) => handleTagAddition(tag)}
                  handleDrag={() => console.log("Drag")}
                />
              ) : (
                tags.slice(0, isTagsExpanded ? tags.length : 3).map(({ id, name, _links }) => (
                  <Link
                    key={id}
                    to={GetLinkToTag(_links.self.href)}
                    style={{
                      display: 'inline',
                    }}
                  >
                    {name}
                  </Link>
                ))
              )}
            </div>
            {!isEditing && tags.length > 3 && (
              <button
                className="show-tags-button"
                style={{
                  cursor: `pointer`,
                  transition: `transform 0.5s ease`,
                }}
                onClick={() => {
                  setExpandedTags((prevExpandedTags) => ({
                    ...prevExpandedTags,
                    [id]: !prevExpandedTags[id],
                  }));

                }}
              >
                <FontAwesomeIcon
                  icon={isTagsExpanded ? faChevronUp : faChevronDown}
                />
              </button>
            )}
          </td>
          <td>
            {isEditing ? (
              <>
                <Button
                  className="admin-button"
                  text="Update"
                  backColor="green"
                  onClick={() => update(id)}
                />
                <Button
                  className="admin-button"
                  text="Cancel"
                  backColor="red"
                  onClick={cancel}
                />
              </>
            ) : (
              <>
                <Button
                  className="admin-button"
                  text="Edit"
                  backColor="blue"
                  onClick={() => edit(id)}
                />
                <Button
                  className="admin-button"
                  text="View"
                  backColor="green"
                  onClick={() => view(id)}
                />
                <Button
                  className="admin-button"
                  text="Delete"
                  backColor="red"
                  onClick={() => handleDeleteCertificate(id, name)}
                />
              </>
            )}

          </td>
        </tr>
      )
    }
  );
  return (
    <div className=''>
      {tagError && <div style={{ color: 'red', marginTop: '8px' }}>{tagError}</div>}
      <table>
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Duration</th>
            <th>Create Date</th>
            <th>Last Update</th>
            <th>Tags</th>
            <th>Buttons</th>
          </tr>
        </thead>
        <tbody>
          {table}
        </tbody>
      </table>
    </div>
  );
}


export default TableData;