import React from 'react'
import PropTypes from 'prop-types'

function Button({ className, backColor, text, onClick, style }) {
  return (
    <button
      className={className}
      onClick={onClick}
      style={style ? style : { backgroundColor: backColor, color: 'white' }}
    >
      {text}
    </button>
  );
}

Button.defaultProps = {
  color: 'steelblue',
  text: 'Button',
  onClick: () => {
    console.log("Click")
  }
}

Button.propTypes = {
  color: PropTypes.string,
  onClick: PropTypes.func,
}

export default Button
