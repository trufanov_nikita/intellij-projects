import React from 'react'
import like from '../assets/like.svg'
import { Link } from 'react-router-dom'
import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { faChevronUp } from '@fortawesome/free-solid-svg-icons';
import formatDateTime from './FormateDateTime';
import OrderCertificate from './OrderCertificate';
const CertificateSmall = ({ certificate }) => {
  const [showAllTags, setShowAllTags] = useState(false);

  const toggleShowAllTags = () => {
    setShowAllTags((prevShowAllTags) => !prevShowAllTags);
  };

  return (
    <div className="coupon">
      <div className="coupon-image-wrapper">
        <img
          src="https://www.blueoshan.com/hubfs/BO_Blog_The-first-page-of-Google-is-Mount-Everest_Cover.jpg"
          alt="coupon-image"
          className="coupon-image"
        />
      </div>

      <div className="coupon-name-wrapper">
        <span className="coupon-name-text">{certificate.name}</span>
        <img src={like} alt="like" className="like-image-coupon" />
      </div>
      <div className="coupon-desc-wrapper">
        <span className="coupon-desc">{certificate.description}</span>
        <time className="coupon-expire" dateTime={certificate.createdDate}>
          {formatDateTime(certificate.createdDate)}
        </time>
      </div>

      <div className="coupon-tags-wrapper">
        <div className='tag-row'>
          {certificate.tags.slice(0, showAllTags ? certificate.tags.length : 3).map(({ id, name, _links }) => (
            <Link to={`/tag/${id}`} className="coupon-category" key={id}>
              {name}
            </Link>
          ))}
        </div>

        {certificate.tags.length > 3 && !showAllTags && (
          <button onClick={toggleShowAllTags}>
            <FontAwesomeIcon icon={faChevronDown} />
          </button>
          // <Button text='...' backColor='#8e75e4' className="show-all-tags" onClick={toggleShowAllTags} />
        )}

        {certificate.tags.length > 3 && showAllTags && (
          <button onClick={toggleShowAllTags}>
            <FontAwesomeIcon icon={faChevronUp} />
          </button>
        )}
      </div>
      <div className="coupon-buy-wrapper">
        <span className="price-text">${certificate.price}</span>
        <OrderCertificate certificate={certificate} />
      </div>
    </div>
  );
};

export default CertificateSmall;