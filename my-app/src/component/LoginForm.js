import React, { useState } from 'react';
import Cookies from 'universal-cookie';
import { useNavigate } from 'react-router-dom';

export default function LoginForm() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();
  const cookie = new Cookies();

  const handleSubmit = async () => {
    try {
      setError('');
      if (!email || !password) {
        setError('Both email and password are required.');
        return;
      }
      const response = await fetch('/auth/authenticate', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      });
      if (response.ok) {
        const data = await response.json();
        const { access_token } = data;
        cookie.set('jwt', access_token);
        cookie.set('user', data.user);
        navigate(`/certificate?page=0`);
      } else {
        setError('Invalid email or password.');
      }
    } catch (err) {
      console.log(err);
      setError('An error occurred. Please try again.');
    }
  };

  return (
    <div className="main">
      <div className="login-wrapper">
        <div className="login-box">
          <span className="login-text">Login</span>
          <div className="field-input-wrapper">
            {error && <div className='login-error-wrapper'>
              <label className="error-message">{error}</label>
            </div>}
            <input
              type="text"
              name="login-text"
              id="login-text"
              placeholder="Login"
              className={`field ${error ? 'error' : ''}`}
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <input
              type="password"
              name="password-text"
              id="password-text"
              placeholder="Password"
              className={`field ${error ? 'error' : ''}`}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <input type="button" value="Login" className="button login-button" onClick={handleSubmit} />
          </div>
        </div>
      </div>
    </div>
  );
}
