import { useState, useEffect } from 'react';
import CustomError from './CustomError';
import Cookies from 'universal-cookie';

function useCertificateOrdering(certificate) {
    const [ordering, setOrdering] = useState(false);
    const [error, setError] = useState(null);
    const [success, setSuccess] = useState(false);
    const [errorState, setErrorState] = useState(false);

    useEffect(() => {
        if (ordering) {
            setOrdering(true);
            const cookies = new Cookies();
            const bearerToken = cookies.get('jwt');
            const user = cookies.get('user');

            if (!bearerToken || !user || !user.id) {
                setError(new CustomError("You are not logged in to make an order", 400));
                setOrdering(false);
                return;
            }

            const headers = {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${bearerToken}`,
            };

            async function orderAndWait() {
                await new Promise(resolve => setTimeout(resolve, 5000));

                fetch(`/order/${user.id}/${certificate.id}`, {
                    method: 'POST',
                    headers,
                })
                    .then(response => {
                        if (!response.ok) {
                            return response.json().then(errorData => {
                                setError(new CustomError(errorData.message, response.status));
                                setErrorState(true);
                                setTimeout(() => {
                                    setErrorState(false);
                                }, 3000);
                            });
                        } else {
                            setSuccess(true);
                            setTimeout(() => {
                                setSuccess(false);
                            }, 3000);
                        }
                    })
                    .catch(error => {
                        setError(new CustomError("An error occurred while processing your order.", 500));
                        setErrorState(true);
                        setTimeout(() => {
                            setErrorState(false);
                        }, 3000);
                    })
                    .finally(() => {
                        setOrdering(false);
                    });
            }
            orderAndWait();
        }
    }, [ordering]);
    return [ordering, setOrdering, error, success, errorState];
}

export default useCertificateOrdering;
