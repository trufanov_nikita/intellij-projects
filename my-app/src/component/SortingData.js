import React, { useState, useEffect, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGear } from '@fortawesome/free-solid-svg-icons';
function SortingData() {
  const initialSortName = localStorage.getItem('sortProperty') || 'id';
  const initialPageSize = localStorage.getItem('pageSize') || '5';
  const initialSortOrder = localStorage.getItem('sortDirection') || 'ASC';

  const [sortName, setSortName] = useState(initialSortName);
  const [pageSize, setPageSize] = useState(initialPageSize);
  const [sortOrder, setSortOrder] = useState(initialSortOrder);
  const [showSortingBox, setShowSortingBox] = useState(false);
  const [iconRotation, setIconRotation] = useState(0);

  const sortingBoxRef = useRef(null);

  useEffect(() => {
    localStorage.setItem('sortProperty', initialSortName);
    localStorage.setItem('pageSize', initialPageSize);
    localStorage.setItem('sortDirection', initialSortOrder);
    const handleClickOutside = (event) => {
      if (sortingBoxRef.current && !sortingBoxRef.current.contains(event.target)) {
        setShowSortingBox(false);
        setIconRotation((prevRotation) => prevRotation + 180);
      }
    };

    document.addEventListener('click', handleClickOutside);
    return () => {
      document.removeEventListener('click', handleClickOutside);
    };
  }, []);

  const handleSave = () => {
    localStorage.setItem('sortProperty', sortName);
    localStorage.setItem('pageSize', pageSize);
    localStorage.setItem('sortDirection', sortOrder);
    window.location.reload();
  };

  const handleClose = () => {
    setSortName(initialSortName);
    setPageSize(initialPageSize);
    setSortOrder(initialSortOrder);
    setShowSortingBox(false);
  };

  return (
    <div className='sorting-container'>
      <button onClick={(event) => {
        event.stopPropagation();
        setShowSortingBox(!showSortingBox);
        setIconRotation((prevRotation) => prevRotation + 180);
      }}>
        <FontAwesomeIcon icon={faGear} style={{ color: "#0f4db8", transform: `rotate(${iconRotation}deg)`, transition: `transform 0.5s ease` }} />
      </button>
      {showSortingBox && (
        <div className='sorting-data' ref={sortingBoxRef}>
          <form className='sorting-form'>
            <label>
              Sort By:
              <select value={sortName} onChange={(e) => setSortName(e.target.value)}>
                <option value="id">ID</option>
                <option value="name">Name</option>
                <option value="price">Price</option>
                <option value="createdDate">Created Date</option>
                <option value="lastModifiedDate">Updated Date</option>
              </select>
            </label>
            <label>
              Page Size:
              <select value={pageSize} onChange={(e) => setPageSize(e.target.value)}>
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="20">20</option>
              </select>
            </label>
            <label>
              Sort Order:
              <select value={sortOrder} onChange={(e) => setSortOrder(e.target.value)}>
                <option value="ASC">ASC</option>
                <option value="DESC">DESC</option>
              </select>
              <div className='form-buttons'>
                <button onClick={handleSave}>Apply</button>
              </div>
            </label>
          </form>
        </div>
      )}
    </div>

  );
}

export default SortingData;
