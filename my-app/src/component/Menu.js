import React, { useState, useRef, useEffect } from 'react';
import { Link } from 'react-router-dom';
import menu from '../assets/menu.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';

function Menu() {
  const [isDropdownOpen, setDropdownOpen] = useState(false);
  const dropdownRef = useRef(null);

  const toggleDropdown = () => {
    setDropdownOpen(!isDropdownOpen);
  };

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setDropdownOpen(false);
      }
    };

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);

  return (
    <div className='menu' ref={dropdownRef}>
      {isDropdownOpen ? (
        <FontAwesomeIcon icon={faXmark} className="dropdown-menu-image" onClick={toggleDropdown} />
      ) : (
        <img
          src={menu}
          alt="menu"
          className="dropdown-menu-image"
          onClick={toggleDropdown}
        ></img>
      )}

      {isDropdownOpen && (
        <div className="dropdown-content">
          <Link to="/certificate">Certificates</Link>
          <Link to="/">Home</Link>
        </div>
      )}
    </div>
  );
}

export default Menu;

