import React, { useState, useEffect } from 'react';
import Button from './Button';
import Cookies from 'universal-cookie';
import Error from './Error';
import CustomError from './CustomError';
import { useNavigate } from 'react-router-dom';
const DeleteConfirmationPopup = ({ tagName, onDelete, onCancel }) => {
    return (
        <div className="popup">
            <div className="popup-content">
                <h3>{`Do you want to delete tag with name: ${tagName}?`}</h3>
                <div className="popup-buttons">
                    <Button text="Yes" backColor="red" onClick={onDelete} />
                    <Button text="No" backColor="green" onClick={onCancel} />
                </div>
            </div>
        </div>
    );
};
function TagMain({ tagNumber }) {
    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [editMode, setEditMode] = useState(false);
    const [editedTagName, setEditedTagName] = useState('');
    const [showDeletePopup, setShowDeletePopup] = useState(false);
    const navigate = useNavigate();
    const getData = async () => {
        try {
            const cookies = new Cookies();
            const bearerToken = cookies.get('jwt');
            const headers = {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${bearerToken}`,
            };
            const response = await fetch(`${tagNumber}`, {
                headers,
            });

            setError(null);
            setLoading(true);



            if (!response.ok) {
                console.log(response);
                let error = await response.json();
                console.log(error);
                throw new CustomError(error.message, response.status);
            }
            let actualData = await response.json();
            setData(actualData);
            setEditedTagName(actualData.name);
        }
        catch (err) {
            setError(err);
        }
        finally {
            setLoading(false);
        }
    }
    useEffect(() => {
        const controller = new AbortController();
        getData();
        return () => {
            controller.abort();
        };
    }, []);

    const handleEdit = () => {
        setEditMode(true);
    };

    const handleSave = async () => {
        try {
            if (editedTagName.length < 1) {
                setError(new CustomError("tag name should be at least 1 character", 400));
                return;
            }
            const cookies = new Cookies();
            const bearerToken = cookies.get('jwt');
            const headers = {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${bearerToken}`,
            };

            const response = await fetch(`${tagNumber}`, {
                method: 'POST',
                headers,
                body: JSON.stringify({ name: editedTagName }),
            });

            if (!response.ok) {
                let error = await response.json();
                throw new CustomError(error.message, response.status);
            }

            setEditMode(false);
            getData();

        } catch (err) {
            setError(err);
        }
    };

    const handleDeleteTag = async () => {
        try {
            const cookies = new Cookies();
            const bearerToken = cookies.get('jwt');
            const headers = {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${bearerToken}`,
            };

            const response = await fetch(`/certificate/tag/${tagNumber}`, {
                method: 'DELETE',
                headers,
            });

            if (!response.ok) {
                let error = await response.json();
                throw new CustomError(error.message, response.status);
            }

            setShowDeletePopup(false);
            navigate('/certificate');
        } catch (err) {
            setError(err);
        }
    };

    const handleCancel = () => {
        setEditMode(false);
        setEditedTagName(data.name);
    };

    return (
        <div className='main'>
            {loading && <div>A moment please...</div>}
            {error && <Error error={error} />}
            <h1>Tag</h1>
            {data && (
                <div className='tag-wrapper'>
                    <div className='tag-data'>
                        <h2>Id: {data.id}</h2>
                        {editMode ? (
                            <input
                                type='text'
                                value={editedTagName}
                                onChange={(e) => setEditedTagName(e.target.value)}
                            />
                        ) : (
                            <h2>Name: {data.name}</h2>
                        )}
                    </div>
                    <div className='tag-buttons'>
                        {editMode ? (
                            <div className='edit-buttons'>
                                <Button text='Save' backColor='green' onClick={handleSave} />
                                <Button text='Cancel' backColor='red' onClick={handleCancel} />
                            </div>
                        ) : (
                            <>
                                <Button text='Edit' backColor='blue' onClick={handleEdit} />
                                <Button
                                    text='Delete'
                                    backColor='red'
                                    onClick={() => setShowDeletePopup(true)}
                                />
                            </>
                        )}
                    </div>
                </div>
            )}
            {showDeletePopup && (
                <DeleteConfirmationPopup
                    tagName={data.name}
                    onDelete={handleDeleteTag}
                    onCancel={() => setShowDeletePopup(false)}
                />
            )}
        </div>
    );
}

export default TagMain;
