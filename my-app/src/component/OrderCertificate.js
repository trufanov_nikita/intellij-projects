import React from 'react';
import Error from './Error';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import useCertificateOrdering from './useCertificateOrdering';

function OrderCertificate({ certificate }) {
    const [ordering, setOrdering, error, success, errorState] = useCertificateOrdering(certificate);

    const orderCertificate = async () => {
        if (!ordering && !errorState && !success) {
            setOrdering(true);
        }
    };

    return (
        <div>
            {error && <Error error={error} />}
            <button
                type="submit"
                className="add-to-cart"
                onClick={orderCertificate}
            >
                {ordering ? <FontAwesomeIcon icon={faSpinner} spin /> : success ? <FontAwesomeIcon icon={faCheck} beat style={{ color: "#4CAF50", }} /> : errorState ? <FontAwesomeIcon icon={faXmark} beat style={{ color: "#dc1709", }} /> : "Add to Cart"}
            </button>
        </div>
    );
}

export default OrderCertificate;
