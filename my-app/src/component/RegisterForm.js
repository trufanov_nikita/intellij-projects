import React, { useState } from 'react';
import Cookies from 'universal-cookie';
import { useNavigate } from 'react-router-dom';

function RegisterForm() {
    const [formData, setFormData] = useState({
        name: '',
        surname: '',
        password: '',
        email: ''
    });

    const [fieldErrors, setFieldErrors] = useState({
        name: '',
        surname: '',
        password: '',
        email: ''
    });

    const [error, setError] = useState(null);
    const [errorTimer, setErrorTimer] = useState(null);
    const cookie = new Cookies();
    const navigate = useNavigate();

    const handleSubmit = async (event) => {
        event.preventDefault();
        setFieldErrors({
            name: '',
            surname: '',
            password: '',
            email: ''
        });
        let hasErrors = false;

        if (!formData.name) {
            setFieldErrors((prevErrors) => ({ ...prevErrors, name: 'Login is required.' }));
            hasErrors = true;
        }
        if (!formData.surname) {
            setFieldErrors((prevErrors) => ({ ...prevErrors, surname: 'First Name is required.' }));
            hasErrors = true;
        }
        if (!formData.password) {
            setFieldErrors((prevErrors) => ({ ...prevErrors, password: 'Password is required.' }));
            hasErrors = true;
        }
        if (!formData.email) {
            setFieldErrors((prevErrors) => ({ ...prevErrors, email: 'Email is required.' }));
            hasErrors = true;
        }

        if (!/^[A-Za-z]+$/.test(formData.name) || !/^[A-Za-z]+$/.test(formData.name)) {
            setFieldErrors((prevErrors) => ({ ...prevErrors, name: 'Name should contain only letters' }));
            hasErrors = true;
        }

        if (!/^[A-Za-z]+$/.test(formData.surname) || !/^[A-Za-z]+$/.test(formData.surname)) {
            setFieldErrors((prevErrors) => ({ ...prevErrors, surname: 'Surname should contain only letters' }));
            hasErrors = true;
        }

        if (!/^[\w.-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,}$/.test(formData.email)) {
            setFieldErrors((prevErrors) => ({ ...prevErrors, email: 'Wrong email format' }));
            hasErrors = true;
        }

        if (formData.password.length < 6) {
            setFieldErrors((prevErrors) => ({ ...prevErrors, password: 'Password length should be more than 6' }));
            hasErrors = true;
        }

        if (hasErrors) {
            clearTimeout(errorTimer);
            setErrorTimer(setTimeout(() => setFieldErrors({
                name: '',
                surname: '',
                password: '',
                email: ''
            }), 5000));
            return;
        }

        setError(null);
        try {
            const response = await fetch('/auth/register', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: formData.name,
                    surname: formData.surname,
                    email: formData.email,
                    password: formData.password,
                    role: 'ADMIN'
                })
            });

            if (!response.ok) {
                throw new Error('Failed to register user.');
            }
            const data = await response.json();
            const token = data.access_token;
            cookie.set('jwt', token);
            cookie.set('user', data.user);
            setFormData({
                name: '',
                surname: '',
                password: '',
                email: ''
            });
            navigate("/certificate");
        } catch (error) {
            setError(error.message);
            console.log(error)
        }
    };
    return (
        <div className="register-wrapper">
            <div className="register-text-wrapper">
                <span className="register-text">Register</span>
            </div>
            <div className="register-box">
                {error && <div className="error-message">{error}</div>}
                <form className="input-container" onSubmit={handleSubmit}>
                    <div className={`label-input ${fieldErrors.name ? 'error' : ''}`}>
                        <label htmlFor="login" className="register-input-label">Name</label>
                        <input
                            type="text"
                            id="login"
                            name="login"
                            className="register-input-field"
                            value={formData.name}
                            onChange={(e) => setFormData({ ...formData, name: e.target.value })}
                        />
                        {fieldErrors.name && <div className="error-text">{fieldErrors.name}</div>}
                    </div>

                    <div className={`label-input ${fieldErrors.surname ? 'error' : ''}`}>
                        <label htmlFor="first-name" className="register-input-label">Surname</label>
                        <input
                            type="text"
                            id="first-name"
                            name="first-name"
                            className="register-input-field"
                            value={formData.surname}
                            onChange={(e) => setFormData({ ...formData, surname: e.target.value })}
                        />
                        {fieldErrors.surname && <div className="error-text">{fieldErrors.surname}</div>}
                    </div>

                    <div className={`label-input ${fieldErrors.password ? 'error' : ''}`}>
                        <label htmlFor="password" className="register-input-label">Password</label>
                        <input
                            type="password"
                            id="password"
                            name="password"
                            className="register-input-field"
                            value={formData.password}
                            onChange={(e) => setFormData({ ...formData, password: e.target.value })}
                        />
                        {fieldErrors.password && <div className="error-text">{fieldErrors.password}</div>}
                    </div>

                    <div className={`label-input ${fieldErrors.email ? 'error' : ''}`}>
                        <label htmlFor="email" className="register-input-label">Email</label>
                        <input
                            type="text"
                            id="email"
                            name="email"
                            className="register-input-field"
                            value={formData.email}
                            onChange={(e) => setFormData({ ...formData, email: e.target.value })}
                        />
                        {fieldErrors.email && <div className="error-text">{fieldErrors.email}</div>}
                    </div>
                    <div className="button-container">
                        <button className="cancel-button">Cancel</button>
                        <button className="submit-button">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default RegisterForm;