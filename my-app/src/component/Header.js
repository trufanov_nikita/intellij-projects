import Button from './Button';
import React, { useState, useEffect } from 'react';
import Cookies from 'universal-cookie';
import { useNavigate } from 'react-router-dom';
import "../assets/style.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';
import SortingData from './SortingData';
import FilterData from './FilterData';
import Menu from './Menu';
function Header() {
  const cookie = new Cookies();
  const [authorized, setAuthorize] = useState(false);
  const [searchData, setSearchData] = useState(null);
  const navigate = useNavigate();
  
  useEffect(() => {
    const jwtCookie = cookie.get('jwt');
    if (jwtCookie != null && jwtCookie !== undefined && jwtCookie !== 'undefined') {
      setAuthorize(true);
    } else {
      setAuthorize(false);
    }
  }, []);

  function search() {
    if (searchData != null) {
      navigate(`/certificate/search/${searchData}`)
    }
  }

  function login() {
    navigate('/login');
  }

  function register() {
    navigate('/register');
  }

  function logout() {
    cookie.set('jwt', undefined);
    cookie.set('user', null);
    setAuthorize(false);
    navigate('/');
  }

  function profile() {
    console.log('profile');
  }

  return (
    <div className="header">
      <div className="head-left">
        <Menu />
      </div>
      <div className="head-center">
        <div className="input-wrapper">

          <SortingData />
          <input type="search" id="searchInput" className="search-input" placeholder="Search" onChange={(e) => setSearchData(e.target.value)}></input>
          <button onClick={() => search()}>
            <FontAwesomeIcon
              icon={faMagnifyingGlass}
              style={{
                color: '#0f4db8',
                transition: `transform 0.5s ease`,
              }} />
          </button>
          {/* <Button style={{
            color: '#fff',
            background: '#195de6',
            borderColor: '#195de6'
          }} onClick={() => search()} /> */}
          <FilterData />
        </div>
      </div>
      {authorized ?
        <div className="head-right">
          <div className="log-sign-wrapper">
            <Button style={{
              color: 'black',
              background: 'none',

              border: 'none'
            }} text="Profile" onClick={() => profile()} />
            <span className="nav"> | </span>
            <Button style={{
              color: 'black',
              background: 'none',

              border: 'none'
            }} text="Logout" onClick={() => logout()} />
          </div>
        </div>
        :
        <div className="head-right">
          <div className="log-sign-wrapper">
            <Button style={{
              color: 'black',
              background: 'none',
              borderColor: '#195de6'
            }} text="Login" onClick={() => login()} />
            <span className="nav"> | </span>
            <Button style={{
              color: 'black',
              background: 'none',
              borderColor: '#195de6'
            }} text="Sign Up" onClick={() => register()} />
          </div>
        </div>
      }
    </div>
  );
}

export default Header
