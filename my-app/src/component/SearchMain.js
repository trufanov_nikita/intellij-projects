import React, { useState, useEffect } from 'react';
import CertificateSmall from './CertificateSmall';
import CustomScrollbar from './CustomScrollbar';
import { debounce } from 'lodash';
import Error from './Error';
import CustomError from './CustomError';

function SearchMain({ search }) {
  const PAGE_SIZE = 5;
  const SORT_PROPERTY = 'id';
  const SORT_DIRECTION = 'ASC';
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [currentPage, setCurrentPage] = useState(0);
  const [totalPages, setTotalPages] = useState(0);
  const [showLoadMore, setShowLoadMore] = useState(true);

  useEffect(() => {
    setCurrentPage(0);
    setData([]);
    setTotalPages(0);
    setError(null);
  }, [search]);

  const debouncedScrollHandler = debounce(() => {
    const currentScrollPosition = window.scrollY;
    const maxScrollPositionReached =
      currentScrollPosition >= maxScrollPosition;
    if (
      maxScrollPositionReached &&
      currentScrollPosition + window.innerHeight >=
      document.documentElement.scrollHeight - scrollThreshold
    ) {
      if (!loading) {
        if (currentPage >= totalPages) {
          setShowLoadMore(false);
          window.removeEventListener('scroll', debouncedScrollHandler);
        } else {
          setCurrentPage((prevPage) => prevPage + 1);
          maxScrollPosition = currentScrollPosition;
        }
      }
    }
  }, 500);

  useEffect(() => {
    const body = document.body;
    if (body.scrollHeight <= window.innerHeight) {
      setShowLoadMore(true);
    }

    window.addEventListener('scroll', debouncedScrollHandler);

    return () => {
      window.removeEventListener('scroll', debouncedScrollHandler);
    };
  }, [debouncedScrollHandler]);

  let maxScrollPosition = 0;
  const scrollThreshold = 300;

  const getData = async (page) => {
    try {
      setLoading(true);
      let response;
      const selectedTags = JSON.parse(localStorage.getItem('selectedTags'));
      if (selectedTags && selectedTags.length > 0) {
        const tagsQueryString = `&` + selectedTags.map((tag) => `tags=${tag.name}`).join('&');
        response = await fetch(
          `/certificate/pagination/search/tag/${search}?page=${page}
          &pageSize=${localStorage.getItem('pageSize') || PAGE_SIZE}
          &sortProperty=${localStorage.getItem('sortProperty') || SORT_PROPERTY}
          &sortDirection=${localStorage.getItem('sortDirection') || SORT_DIRECTION}
          ${tagsQueryString}`
        );
      } else {
        response = await fetch(
          `/certificate/pagination/search/${search}?page=${page}
          &pageSize=${localStorage.getItem('pageSize')}
          &sortProperty=${localStorage.getItem('sortProperty')}
          &sortDirection=${localStorage.getItem('sortDirection')}`
        );
      }

      if (!response.ok) {
        let error = await response.json();
        throw new CustomError(error.message, response.status);
      }

      let actualData = await response.json();

      setTotalPages(actualData.totalPages);
      if (!actualData.data.content.length > 0) {
        throw new CustomError('No data with these tags', 404);
      }
      setData((prevData) => [...prevData, ...actualData.data.content]);
      setError(null);
    } catch (err) {
      setError(err);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    const controller = new AbortController();
    if (currentPage === 0 || currentPage < totalPages) {
      getData(currentPage);
    }
    return () => {
      controller.abort();
    };
  }, [search, currentPage]);

  useEffect(() => {
    setShowLoadMore(false);
    const body = document.body;
    console.log(currentPage + " " + totalPages);
    if (body.scrollHeight <= window.innerHeight && currentPage < totalPages - 1) {
      setShowLoadMore(true);
    }

  }, [data]);

  const handleLoadMore = () => {
    setCurrentPage((prevPage) => prevPage + 1);
  };

  return (
    <div className='search-main'>
      {error && <Error error={error} />}
      {data && (
        <div className='coupon-wrapper'>
          {data.map((certificate) => {
            return <CertificateSmall certificate={certificate} />;
          })}
          <CustomScrollbar totalPages={totalPages} currentPage={currentPage} />
        </div>
      )}
      {showLoadMore && (
        <div className='load-more'>
          <button className='pagination-button' onClick={handleLoadMore}>
            Load More
          </button>
        </div>
      )}
    </div>
  );
}

export default SearchMain;
