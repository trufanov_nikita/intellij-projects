function formatDateTime(dateTimeString) {
  const [datePart, timePart] = dateTimeString.split(' ');
  const formattedDate = `${datePart} ${timePart}`;
  return formattedDate;
}

export default formatDateTime;

