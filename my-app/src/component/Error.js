import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleXmark } from '@fortawesome/free-solid-svg-icons';
import { useEffect } from 'react';
function Error({ error }) {
    console.log(error);
    const [isVisible, setIsVisible] = useState(true);
    const result = {
        message: 'Unhandled Error',
        status: '401',
    };

    if (error) {
        if (error.message) {
            result.message = error.message;
        }
        if (error.status && error.status === 403) {
            result.message = `You dont have permission to this operation`;
        }
        if (error.status) {
            result.status = error.status;
        }
    }

    const handleClose = () => {
        setIsVisible(false);
    };

    useEffect(() => {
        setIsVisible(true);
    }, [error])

    useEffect(() => {
        let timer;

        if (isVisible) {
            timer = setTimeout(() => {
                setIsVisible(false);
            }, 10000);
        }

        return () => {
            clearTimeout(timer);
        };
    }, [isVisible]);

    return isVisible ? (
        <div className='error-wrapper'>
            <button className='close-button' onClick={handleClose}>
                <FontAwesomeIcon icon={faCircleXmark} />
            </button>

            <div className='error-text'>
                <span>Status: {result.status}</span>
                <span>Message: {result.message}</span>
            </div>

        </div>
    ) : null;
}

export default Error;