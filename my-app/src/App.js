import React from 'react';
import Header from './component/Header';
import Footer from './component/Footer';
import AppMain from './component/AppMain';

function App() {
  return (
    <React.StrictMode>
      <Header />
      <AppMain />
      <Footer />
    </React.StrictMode>
  );
}

export default App;
