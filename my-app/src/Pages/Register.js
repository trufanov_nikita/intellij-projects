import Footer from "../component/Footer";
import Header from "../component/Header";
import RegisterForm from "../component/RegisterForm";
import "../assets/style.css"

function Register() {
    return (
        <>
            <Header />
            <div className="main">
                <RegisterForm />
            </div>
            <Footer />
        </>

    )
}

export default Register;