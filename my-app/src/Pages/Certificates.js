import React from 'react';
import Header from '../component/Header';
import Main from '../component/Main';
import Footer from '../component/Footer';
import { useLocation } from 'react-router-dom';

function Certificates() {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const page = searchParams.get('page');
  return (
    <React.StrictMode>
      <Header />
      <Main page={parseInt(page)} /> {/* Parse the 'page' parameter to an integer */}
      <Footer />
    </React.StrictMode>
  );
}

export default Certificates;