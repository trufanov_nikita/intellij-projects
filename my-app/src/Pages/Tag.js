import Footer from "../component/Footer";
import Header from "../component/Header";
import { useParams } from 'react-router-dom';
import TagMain from "../component/TagMain";

function Tag() {
    const { tagNumber } = useParams();
    return (
        <div>
            <Header />
            <TagMain tagNumber={tagNumber} />
            <Footer />
        </div>
    )
}

export default Tag;
