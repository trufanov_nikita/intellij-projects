import Header from '../component/Header';
import Footer from '../component/Footer';
import React from 'react';
import { useParams } from 'react-router-dom';
import CertificateMain from '../component/CertificateMain';

function Certificate() {
    const { certificateNumber } = useParams();
    console.log(certificateNumber);
    return (
        <>
            <Header />
            <CertificateMain certificateNumber={certificateNumber} />
            <Footer />
        </>
    )
}

export default Certificate;

