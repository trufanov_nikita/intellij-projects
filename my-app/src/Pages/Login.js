import Footer from "../component/Footer";
import Header from "../component/Header";
import LoginForm from "../component/LoginForm";

function Login() {
    return (
        <>
            <Header />
            <LoginForm />
            <Footer />
        </>
    )
}

export default Login;