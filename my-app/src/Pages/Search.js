import { useParams } from 'react-router-dom';
import React from 'react';
import Header from '../component/Header';
import SearchMain from '../component/SearchMain';
import Footer from '../component/Footer';

function Search() {
    const { searchCertificate } = useParams();
    return (
        <div>
            <Header />
            <SearchMain search={searchCertificate} />
            <Footer />

        </div>
    )
}

export default Search;