import React from 'react';
import { createRoot } from 'react-dom/client';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import App from './App';
import Certificates from './Pages/Certificates';
import Certificate from './Pages/Certificate';
import Login from './Pages/Login';
import Register from './Pages/Register';
import Tag from './Pages/Tag';
import Search from './Pages/Search';

const rootElement = document.getElementById('root');

createRoot(rootElement).render(
  <div className='body'>
    <Router>
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/certificate" element={<Certificates />} />
        <Route path="/tag/:tagNumber" element={<Tag />} />
        <Route path="/certificate/search/:searchCertificate" element={<Search />} />
        <Route path="/certificate/:certificateNumber" element={<Certificate />} />
      </Routes>
    </Router>
  </div>
);








