package com.epam.esm;

import com.epam.esm.controller.PaginationCertificateController;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.repository.CertificateRepository;
import com.epam.esm.service.PaginationCertificateService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@ExtendWith(MockitoExtension.class)
public class TestPaginationCertificateService {

    @Mock
    private CertificateRepository certificateRepository;

    @InjectMocks
    private PaginationCertificateService paginationCertificateService;

    private GiftCertificate certificate = new GiftCertificate("name", "description", 2.2f, 30, null);

    private final int pageSize = 5;

    private final String prevLink = "prevPage";

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 5})
    void testGetCertificates(int page){
        Link prevPage = linkTo(methodOn(PaginationCertificateController.class).getAll(page -1)).withRel(prevLink);
        PageRequest pageRequest = PageRequest.of(page,pageSize);
        List<GiftCertificate> content = List.of(new GiftCertificate(), new GiftCertificate());
        Page<GiftCertificate> list = new PageImpl<>(content);
        when(certificateRepository.findAll(pageRequest)).thenReturn(list);

        ResponseEntity<Object> response = paginationCertificateService.getCertificates(page);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Object responseBody = response.getBody();
        assertNotNull(responseBody);
        assertTrue(responseBody instanceof CollectionModel);
        CollectionModel<GiftCertificate> result = (CollectionModel<GiftCertificate>) responseBody;
        Collection<GiftCertificate> resultList = result.getContent();
        assertNotNull(resultList);
        assertEquals(content.size(), resultList.size());
        if (page > 0) {
            List<Link> links = result.getLinks().toList();
            assertNotNull(links);
            boolean hasPrevLink = links.stream()
                    .anyMatch(link -> link.equals(prevPage));
            assertTrue(hasPrevLink);
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 5})
    void testFindAllOrderByNameAsc(int page){
        Link prevPage = linkTo(methodOn(PaginationCertificateController.class).getByNameAsc(page -1)).withRel(prevLink);
        PageRequest pageRequest = PageRequest.of(page,pageSize);
        List<GiftCertificate> content = List.of(new GiftCertificate(), new GiftCertificate());
        when(certificateRepository.findAllByOrderByNameAsc(pageRequest)).thenReturn(content);

        ResponseEntity<Object> response = paginationCertificateService.findAllOrderByNameAsc(page);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Object responseBody = response.getBody();
        assertNotNull(responseBody);
        assertTrue(responseBody instanceof CollectionModel);
        CollectionModel<GiftCertificate> result = (CollectionModel<GiftCertificate>) responseBody;
        Collection<GiftCertificate> resultList = result.getContent();
        assertNotNull(resultList);
        assertEquals(content.size(), resultList.size());
        if (page > 0) {
            List<Link> links = result.getLinks().toList();
            assertNotNull(links);
            boolean hasPrevLink = links.stream()
                    .anyMatch(link -> link.equals(prevPage));
            assertTrue(hasPrevLink);
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 5})
    void testFindAllOrderByNameDesc(int page) {
        Link prevPage = linkTo(methodOn(PaginationCertificateController.class).getByNameDesc(page -1)).withRel(prevLink);
        PageRequest pageRequest = PageRequest.of(page, pageSize);
        List<GiftCertificate> content = List.of(certificate);
        when(certificateRepository.findAllByOrderByNameDesc(pageRequest)).thenReturn(content);

        ResponseEntity<Object> response = paginationCertificateService.findAllOrderByNameDesc(page);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Object responseBody = response.getBody();
        assertNotNull(responseBody);
        assertTrue(responseBody instanceof CollectionModel);
        CollectionModel<GiftCertificate> result = (CollectionModel<GiftCertificate>) responseBody;
        Collection<GiftCertificate> resultList = result.getContent();
        assertNotNull(resultList);
        assertEquals(content.size(), resultList.size());
        if (page > 0) {
            List<Link> links = result.getLinks().toList();
            assertNotNull(links);
            boolean hasPrevLink = links.stream()
                    .anyMatch(link -> link.equals(prevPage));
            assertTrue(hasPrevLink);
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 5})
    void testFindAllOrderByDateAsc(int page){
        Link prevPage = linkTo(methodOn(PaginationCertificateController.class).getByDateAsc(page -1)).withRel(prevLink);
        PageRequest pageRequest = PageRequest.of(page,pageSize);
        List<GiftCertificate> content = List.of(new GiftCertificate(), new GiftCertificate());
        when(certificateRepository.findAllByOrderByCreateDateAsc(pageRequest)).thenReturn(content);

        ResponseEntity<Object> response = paginationCertificateService.findAllOrderByDateAsc(page);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Object responseBody = response.getBody();
        assertNotNull(responseBody);
        assertTrue(responseBody instanceof CollectionModel);
        CollectionModel<GiftCertificate> result = (CollectionModel<GiftCertificate>) responseBody;
        Collection<GiftCertificate> resultList = result.getContent();
        assertNotNull(resultList);
        assertEquals(content.size(), resultList.size());
        if (page > 0) {
            List<Link> links = result.getLinks().toList();
            assertNotNull(links);
            boolean hasPrevLink = links.stream()
                    .anyMatch(link -> link.equals(prevPage));
            assertTrue(hasPrevLink);
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 5})
    void testFindAllOrderByDateDesc(int page){
        Link prevPage = linkTo(methodOn(PaginationCertificateController.class).getByDateDesc(page -1)).withRel(prevLink);
        PageRequest pageRequest = PageRequest.of(page,pageSize);
        List<GiftCertificate> content = List.of(new GiftCertificate(), new GiftCertificate());
        when(certificateRepository.findAllByOrderByCreateDateDesc(pageRequest)).thenReturn(content);

        ResponseEntity<Object> response = paginationCertificateService.findAllOrderByDateDesc(page);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Object responseBody = response.getBody();
        assertNotNull(responseBody);
        assertTrue(responseBody instanceof CollectionModel);
        CollectionModel<GiftCertificate> result = (CollectionModel<GiftCertificate>) responseBody;
        Collection<GiftCertificate> resultList = result.getContent();
        assertNotNull(resultList);
        assertEquals(content.size(), resultList.size());
        if (page > 0) {
            List<Link> links = result.getLinks().toList();
            assertNotNull(links);
            boolean hasPrevLink = links.stream()
                    .anyMatch(link -> link.equals(prevPage));
            assertTrue(hasPrevLink);
        }
    }
}
