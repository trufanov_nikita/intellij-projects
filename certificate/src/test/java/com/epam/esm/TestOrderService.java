package com.epam.esm;

import com.epam.esm.entity.Order;
import com.epam.esm.exception.ApiException;
import com.epam.esm.repository.OrderRepository;
import com.epam.esm.service.OrderService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.powermock.api.mockito.PowerMockito.when;

@ExtendWith(MockitoExtension.class)
public class TestOrderService {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderService orderService;

    private final int pageSize = 5;

    private final int page = 1;

    @Test
    void testFindUserIdWithBiggestSumOfOrders() {
        List<Object[]> list = new ArrayList<>();
        list.add(new Object[]{1L, 2L});
        when(orderRepository.findUserIdWithBiggestSumOfOrders()).thenReturn(list);
        try {
            assertEquals(1L, (long) orderService.findUserIdWithBiggestSumOfOrders());
        } catch (ApiException e) {
            fail();
        }
    }

    @Test
    void testFindUserIdWithBiggestSumOfOrdersNoOrders() {
        List<Object[]> list = new ArrayList<>();
        when(orderRepository.findUserIdWithBiggestSumOfOrders()).thenReturn(list);
        assertThrows(ApiException.class, () -> orderService.findUserIdWithBiggestSumOfOrders());
    }

    @Test
    void testFindAll(){
        PageRequest pageRequest = PageRequest.of(page,pageSize);
        pageRequest.withSort(Sort.by("purchase_date"));
        List<Order> content = List.of(new Order(), new Order());
        Page<Order> list = new PageImpl<>(content);
        when(orderRepository.findAll(pageRequest)).thenReturn(list);
        assertEquals(HttpStatus.OK, orderService.findAll(1).getStatusCode());
    }

    @Test
    void testFindByUserId() throws ApiException {
        PageRequest pageRequest = PageRequest.of(page,pageSize);
        pageRequest.withSort(Sort.by("purchase_date"));
        List<Order> content = List.of(new Order(), new Order());
        Page<Order> list = new PageImpl<>(content);
        when(orderRepository.findByUserId(1L, pageRequest)).thenReturn(content);
        assertEquals(HttpStatus.OK, orderService.findByUserId(1L,1).getStatusCode());
    }
}
