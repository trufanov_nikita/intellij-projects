package com.epam.esm;

import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.exception.ErrorResponse;
import com.epam.esm.repository.CertificateRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.service.CertificateService;
import com.epam.esm.service.OrderService;
import com.epam.esm.service.TagService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TestCertificateService {
    @Mock
    private CertificateRepository certificateRepository;

    @Mock
    private OrderService orderService;

    @Mock
    private TagRepository tagRepository;

    @InjectMocks
    private CertificateService certificateService;

    @Mock
    private TagService tagService;

    private static final String alreadyExists = "GiftCertificate already exists with id :";

    private static final String incorrectValues = "Some values was null or incorrect";

    private static final String tagNotFound = "Not tag with id :";

    private static final String certificateNotFound = "GiftCertificate not found with id: ";

    private static final String certificateNotFoundWithDesc = "Certificate not found with description: ";

    private static final String certificateNotFoundWithName = "Certificate not found with name: ";

    private static final String getCertificateNotFoundWithTags = "GiftCertificate not found with tags that has name ";

    private GiftCertificate giftCertificate;

    private Set<Tag> tags = new HashSet<>();

    private Tag tag;

    private List<GiftCertificate> certificates = new ArrayList<>();

    private ResponseEntity<ErrorResponse> expectedIncorrectData = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(HttpStatus.BAD_REQUEST, incorrectValues));

    private ResponseEntity<ErrorResponse> expectedNotFound = ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, certificateNotFound + 1));

    private ResponseEntity<ErrorResponse> expectedNotFoundTagName = ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, getCertificateNotFoundWithTags + "name"));

    private ResponseEntity<ErrorResponse> expectedTagConflict = ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse(HttpStatus.CONFLICT, tagNotFound + 2));

    private ResponseEntity<ErrorResponse> expectedTagNotFound = ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, tagNotFound + 2));

    private ResponseEntity<ErrorResponse> expectedNotFoundName = ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, certificateNotFoundWithName + "name"));

    private ResponseEntity<ErrorResponse> expectedNotFoundDesc = ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, certificateNotFoundWithDesc + "name"));

    private ResponseEntity<Object> expectedConflict;

    @BeforeEach
    public void setup() {
        tag = new Tag("name");
        tag.setId(2L);
        tags.add(tag);

        giftCertificate = new GiftCertificate("name", "description", 2.2f, 30, tags);
        giftCertificate.setId(1L);
        certificates = new ArrayList<>();
        certificates.add(giftCertificate);
        expectedConflict = ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse(HttpStatus.CONFLICT, alreadyExists + giftCertificate.getId()));
    }

    @Test
    void testFindAllOrderByNameAsc() {
        List<GiftCertificate> expected = new ArrayList<>();
        expected.add(new GiftCertificate());
        when(certificateRepository.findAllByOrderByNameAsc()).thenReturn(certificates);
        assertEquals(1, certificateService.findAllOrderByNameAsc().size());
    }

    @Test
    void testAddCertificateBadInputs() {
        when(certificateRepository.existsById(giftCertificate.getId())).thenReturn(true);
        assertEquals(expectedConflict, certificateService.addCertificate(giftCertificate));
        when(certificateRepository.existsById(giftCertificate.getId())).thenReturn(false);
        giftCertificate.setName(null);
        giftCertificate.setDescription(null);
        giftCertificate.setDuration(0);
        assertEquals(expectedIncorrectData, certificateService.addCertificate(giftCertificate));
        giftCertificate.setName("name");
        giftCertificate.setDescription("desc");
        giftCertificate.setDuration(2);
        when(certificateRepository.existsById(giftCertificate.getId())).thenReturn(false);
        when(tagService.getTag(tag.getId())).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        assertEquals(expectedTagNotFound, certificateService.addCertificate(giftCertificate));
    }

    @Test
    void testAddCertificateCorrect() {
        giftCertificate.setCreateDate(null);
        giftCertificate.setLastUpdateDate(null);
        ResponseEntity expected = new ResponseEntity(giftCertificate, HttpStatus.CREATED);
        when(certificateRepository.existsById(giftCertificate.getId())).thenReturn(false);
        when(tagService.getTag(tag.getId())).thenReturn(new ResponseEntity<>(tag, HttpStatus.OK));
        when(certificateRepository.save(giftCertificate)).thenReturn(giftCertificate);
        assertEquals(expected, certificateService.addCertificate(giftCertificate));
    }

    @Test
    void testGetCertificates() {
        List<GiftCertificate> expected = new ArrayList<>();
        expected.add(giftCertificate);
        when(certificateRepository.findAll()).thenReturn(expected);
        assertEquals(expected, certificateService.getCertificates());
    }

    @Test
    void testFindAllOrderByNameDesc() {
        List<GiftCertificate> expected = new ArrayList<>();
        expected.add(giftCertificate);
        when(certificateRepository.findAllByOrderByNameDesc()).thenReturn(expected);
        assertEquals(1, certificateService.findAllOrderByNameDesc().size());
    }

    @Test
    void testFindAllOrderByDateAsc() {
        List<GiftCertificate> expected = new ArrayList<>();
        expected.add(giftCertificate);
        when(certificateRepository.findAllByOrderByCreateDateAsc()).thenReturn(expected);
        assertEquals(1, certificateService.findAllOrderByDateAsc().size());
    }

    @Test
    void testFindAllOrderByDateDesc() {
        List<GiftCertificate> expected = new ArrayList<>();
        expected.add(giftCertificate);
        when(certificateRepository.findAllByOrderByCreateDateDesc()).thenReturn(expected);
        assertEquals(1, certificateService.findAllOrderByDateDesc().size());
    }

    @Test
    void testGetCertificateEmpty() {
        when(certificateRepository.findById(1L)).thenReturn(Optional.empty());
        assertEquals(expectedNotFound.toString(), certificateService.getCertificate(1L).toString());
    }

    @Test
    void testGetCertificateCorrect() {
        ResponseEntity expected = new ResponseEntity(giftCertificate, HttpStatus.OK);
        when(certificateRepository.findById(1L)).thenReturn(Optional.of(giftCertificate));
        assertEquals(expected, certificateService.getCertificate(1L));
    }

    @Test
    void testDeleteCertificateNotFound() {
        Long id = 1L;
        when(certificateRepository.existsById(id)).thenReturn(false);
        assertEquals(expectedNotFound, certificateService.deleteCertificate(id));
    }

    @Test
    void testDeleteCertificateCorrect() {
        Long id = 1L;
        when(certificateRepository.existsById(id)).thenReturn(true);
        when(orderService.deleteCertificateFromOrders(id)).thenReturn(null);
        doNothing().when(certificateRepository).deleteById(id);
        assertEquals(new ResponseEntity<>(HttpStatus.NO_CONTENT), certificateService.deleteCertificate(id));
    }

    @Test
    void testEditCertificateNotFound() {
        Long id = 1L;
        when(certificateRepository.existsById(id)).thenReturn(false);
        assertEquals(expectedNotFound, certificateService.editCertificate(id, giftCertificate));
    }

    @Test
    void testEditCertificateCorrect() {
        CertificateService spy = Mockito.spy(certificateService);
        Long id = 1L;
        ResponseEntity response = new ResponseEntity(giftCertificate, HttpStatus.OK);
        when(certificateRepository.existsById(id)).thenReturn(true);
        when(certificateRepository.save(any(GiftCertificate.class))).thenReturn(giftCertificate);
        Mockito.doReturn(new ResponseEntity<>(giftCertificate, HttpStatus.OK)).when(spy).getCertificate(Mockito.any());
        when(tagService.getTag(Mockito.anyLong())).thenReturn(new ResponseEntity<>(tag, HttpStatus.OK));
        assertEquals(response, spy.editCertificate(id, giftCertificate));
    }

    @Test
    void testEditCertificateNullData() {
        CertificateService spy = Mockito.spy(certificateService);

        Long id = 1L;
        ResponseEntity response = new ResponseEntity(giftCertificate, HttpStatus.OK);
        when(certificateRepository.existsById(id)).thenReturn(true);
        when(certificateRepository.save(any(GiftCertificate.class))).thenReturn(giftCertificate);
        Mockito.doReturn(new ResponseEntity<>(giftCertificate, HttpStatus.OK)).when(spy).getCertificate(Mockito.any());
        assertEquals(response, spy.editCertificate(id, new GiftCertificate()));
    }

    @Test
    void testFindByTagNameNullInput() {
        assertEquals(expectedIncorrectData, certificateService.findByTagName(null));
    }

    @Test
    void testFindByTagNameNullList() {
        String name = "name";
        when(certificateRepository.findByTags_Name(name)).thenReturn(null);
        assertEquals(expectedNotFoundTagName, certificateService.findByTagName(name));
    }

    @Test
    void testFindByTagNameCorrect() {
        ResponseEntity expected = new ResponseEntity(certificates, HttpStatus.OK);
        String name = "name";
        when(certificateRepository.findByTags_Name(name)).thenReturn(certificates);
        assertEquals(expected, certificateService.findByTagName(name));
    }

    @Test
    void testFindByNameNullInput() {
        assertEquals(expectedIncorrectData, certificateService.findByName(null));
    }

    @Test
    void testFindByNameNullList() {
        String name = "name";
        when(certificateRepository.findByName(name)).thenReturn(null);
        assertEquals(expectedNotFoundName, certificateService.findByName(name));
    }

    @Test
    void testFindByNameCorrect() {
        ResponseEntity expected = new ResponseEntity(certificates, HttpStatus.OK);
        String name = "name";
        when(certificateRepository.findByName(name)).thenReturn(certificates);
        assertEquals(expected, certificateService.findByName(name));
    }

    @Test
    void testFindByNameLikeNullInput() {
        assertEquals(expectedIncorrectData, certificateService.findByNameLike(null));
    }

    @Test
    void testFindByNameLikeNullList() {
        String name = "name";
        when(certificateRepository.findByNameLike("%" + name + "%")).thenReturn(null);
        assertEquals(expectedNotFoundName, certificateService.findByNameLike(name));
    }

    @Test
    void testFindByNameLikeCorrect() {
        ResponseEntity expected = new ResponseEntity(certificates, HttpStatus.OK);
        String name = "name";
        when(certificateRepository.findByNameLike("%" + name + "%")).thenReturn(certificates);
        assertEquals(expected, certificateService.findByNameLike(name));
    }


    @Test
    void testFindByDescriptionNullInput() {
        assertEquals(expectedIncorrectData, certificateService.findByDescription(null));
    }

    @Test
    void testFindByDescriptionNullList() {
        String name = "name";
        when(certificateRepository.findByDescription(name)).thenReturn(null);
        assertEquals(expectedNotFoundDesc, certificateService.findByDescription(name));
    }

    @Test
    void testFindByDescriptionCorrect() {
        ResponseEntity expected = new ResponseEntity(certificates, HttpStatus.OK);
        String name = "name";
        when(certificateRepository.findByDescription(name)).thenReturn(certificates);
        assertEquals(expected, certificateService.findByDescription(name));
    }

    @Test
    void testFindByDescriptionLikeNullInput() {
        assertEquals(expectedIncorrectData, certificateService.findByDescriptionLike(null));
    }

    @Test
    void testFindByDescriptionLikeNullList() {
        String name = "name";
        when(certificateRepository.findByDescriptionLike("%" + name + "%")).thenReturn(null);
        assertEquals(expectedNotFoundDesc.getBody(), certificateService.findByDescriptionLike(name).getBody());
    }

    @Test
    void testFindByDescriptionLikeCorrect() {
        ResponseEntity expected = new ResponseEntity(certificates, HttpStatus.OK);
        String name = "name";
        when(certificateRepository.findByDescriptionLike("%" + name + "%")).thenReturn(certificates);
        assertEquals(expected, certificateService.findByDescriptionLike(name));
    }

    @Test
    void testDeleteTagInAllCertificatesNotFound(){
        when(tagService.getTag(tag.getId())).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        assertEquals(expectedTagNotFound, certificateService.deleteTagInAllCertificates(tag.getId()));
    }

    @Test
    void testDeleteTagInAllCertificatesCorrect(){
        List<GiftCertificate>list = new ArrayList<>();
        when(tagService.getTag(tag.getId())).thenReturn(new ResponseEntity<>(tag, HttpStatus.OK));
        when(certificateRepository.findByTags_Id(tag.getId())).thenReturn(list);
        when(tagService.deleteTag(tag.getId())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        assertEquals(new ResponseEntity<>(HttpStatus.NO_CONTENT), certificateService.deleteTagInAllCertificates(tag.getId()));
    }

    @Test
    void testFindByTagsCorrect(){
        Set<String> tagsNames = new HashSet<>();
        when(certificateRepository.findByTags(tagsNames, tagsNames.size())).thenReturn(null);
        assertEquals(null, certificateService.findByTags(tagsNames));
    }
}
