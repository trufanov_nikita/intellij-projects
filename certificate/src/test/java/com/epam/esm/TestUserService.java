package com.epam.esm;

import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.entity.User;
import com.epam.esm.exception.ApiException;
import com.epam.esm.exception.UserException;
import com.epam.esm.repository.OrderRepository;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.service.CertificateService;
import com.epam.esm.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.powermock.api.mockito.PowerMockito.when;

@ExtendWith(MockitoExtension.class)
public class TestUserService {

    @Mock
    private UserRepository userRepository;

    @Mock
    private CertificateService certificateService;

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private UserService userService;

    private String noUser = "No user with this id: ";

    private String emailBusy = "Email is busy";

    private String someFieldsIncorrect = "Some fields are incorrect";

    private String certificateNotFound = "Certificate Not found";

    private String purchaseCompleted = "Purchase completed";

    private User user = new User(1L, "name", "surname", "email@gmail.com");

    private Set<Tag> tags = new HashSet<>();

    private GiftCertificate certificate = new GiftCertificate("name", "desc", 22f, 22, tags);

    private User incorrectUser = new User(1L, "name", "surname", "email");

    private List<User> users = List.of(user);

    @Test
    void testFindAll(){
        when(userRepository.findAll()).thenReturn(users);
        assertEquals(users, userService.findAll().getBody());
        assertEquals(HttpStatus.OK, userService.findAll().getStatusCode());
    }

    @Test
    void testFindByIdNotFound(){
        when(userRepository.existsById(user.getId())).thenReturn(false);
        assertThrows(UserException.class, () -> userService.findById(user.getId()));
    }

    @Test
    void testFindById() throws UserException {
        when(userRepository.existsById(user.getId())).thenReturn(true);
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        assertEquals(user, userService.findById(user.getId()).getBody());
    }

    @Test
    void testCreateExists(){
        when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));
        assertThrows(UserException.class, () -> userService.create(user));
    }

    @Test
    void testCreate() throws UserException {
        when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.ofNullable(null));
        when(userRepository.save(user)).thenReturn(user);
        assertEquals(HttpStatus.CREATED, userService.create(user).getStatusCode());
    }

    @Test
    void testCreateInvalidData() {
        assertThrows(UserException.class, () -> userService.create(incorrectUser));
    }

    @Test
    void testCreateInvalidData2() {
        incorrectUser.setName(null);
        assertThrows(UserException.class, () -> userService.create(incorrectUser));
    }

    @Test
    void testOrderUserNotFound(){
        when(userRepository.existsById(user.getId())).thenReturn(false);
        assertThrows(UserException.class, () -> userService.order(user.getId(), 1L));
    }

    @Test
    void testOrderCertificateNotFound(){
        when(userRepository.existsById(user.getId())).thenReturn(true);
        when(certificateService.getCertificate(1L)).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        assertThrows(ApiException.class, () -> userService.order(user.getId(), 1L));
    }

    @Test
    void testOrder() throws ApiException {
        when(userRepository.existsById(user.getId())).thenReturn(true);
        when(certificateService.getCertificate(1L)).thenReturn(new ResponseEntity<>(certificate, HttpStatus.OK));
        when(orderRepository.existsById(Mockito.any())).thenReturn(false);
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        when(userRepository.save(user)).thenReturn(user);
        assertEquals(HttpStatus.OK, userService.order(user.getId(), 1L).getStatusCode());
    }

    @Test
    void testOrderExists() {
        when(userRepository.existsById(user.getId())).thenReturn(true);
        when(certificateService.getCertificate(1L)).thenReturn(new ResponseEntity<>(certificate, HttpStatus.OK));
        when(orderRepository.existsById(Mockito.any())).thenReturn(true);
        assertThrows(ApiException.class, () ->  userService.order(user.getId(), 1L).getStatusCode());
    }
}
