package com.epam.esm;

import com.epam.esm.entity.Tag;
import com.epam.esm.exception.ErrorResponse;
import com.epam.esm.repository.CertificateRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.service.CertificateService;
import com.epam.esm.service.TagService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TestTagService {
    @Mock
    private CertificateRepository certificateRepository;

    @Mock
    private TagRepository tagRepository;

    @Mock
    private CertificateService certificateService;

    @InjectMocks
    private TagService tagService;

    private String notFound = "No tag with id :";

    private String alreadyExists = "Tag already exists with this id:";

    private Tag tag = new Tag(1L, "name");

    private ResponseEntity expectedConflict = ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse(HttpStatus.CONFLICT, alreadyExists + tag.getId()));

    private ResponseEntity expectedNotFound = ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, notFound + tag.getId()));;

    private ResponseEntity expectedCorrect = new ResponseEntity<>(tag, HttpStatus.OK);;

    private ResponseEntity expectedCorrectNoContent = new ResponseEntity(HttpStatus.NO_CONTENT);

    @BeforeEach
    void setup() {

    }

    @Test
    void testAddTagConflict() {
        when(tagRepository.existsById(tag.getId())).thenReturn(true);
        assertEquals(expectedConflict, tagService.addTag(tag));
    }

    @Test
    void testAddTagCorrect() {
        when(tagRepository.existsById(tag.getId())).thenReturn(false);
        when(tagRepository.save(any())).thenReturn(tag);
        assertEquals(expectedCorrect, tagService.addTag(tag));
    }

    @Test
    void testGetTagCorrect() {
        when(tagRepository.existsById(tag.getId())).thenReturn(true);
        when(tagRepository.findById(tag.getId())).thenReturn(Optional.of(tag));
        assertEquals(expectedCorrect, tagService.getTag(tag.getId()));
    }

    @Test
    void testGetTagNotFound() {
        when(tagRepository.existsById(tag.getId())).thenReturn(false);
        assertEquals(expectedNotFound, tagService.getTag(tag.getId()));
    }

    @Test
    void testEditTagNotFound() {
        when(tagRepository.existsById(tag.getId())).thenReturn(false);
        assertEquals(expectedNotFound, tagService.editTag(tag.getId(), tag));
    }

    @Test
    void testEditTagCorrect() {
        when(tagRepository.existsById(tag.getId())).thenReturn(true);
        when(tagRepository.save(tag)).thenReturn(tag);
        assertEquals(expectedCorrect, tagService.editTag(tag.getId(), tag));
    }

    @Test
    void testDeleteTagNotFound() {
        when(tagRepository.existsById(tag.getId())).thenReturn(false);
        assertEquals(expectedNotFound, tagService.deleteTag(tag.getId()));
    }

    @Test
    void testDeleteTagCorrect() {
        when(tagRepository.existsById(tag.getId())).thenReturn(true);
        doNothing().when(tagRepository).deleteById(tag.getId());
        assertEquals(expectedCorrectNoContent, tagService.deleteTag(tag.getId()));
    }

    @Test
    void testGetTags(){
        List<Tag> list = new ArrayList<>();
        List<Tag> listExpected = new ArrayList<>();
        list.add(tag);
        listExpected.add(tag);
        listExpected.forEach(x-> x.buildLinks());
        when(tagRepository.findAll()).thenReturn(list);
        assertEquals(listExpected, tagService.getTags());
    }
}
