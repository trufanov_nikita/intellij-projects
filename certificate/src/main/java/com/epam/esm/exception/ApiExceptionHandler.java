package com.epam.esm.exception;

import io.jsonwebtoken.MalformedJwtException;
import org.hibernate.HibernateException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@ControllerAdvice
@Order(value = Ordered.HIGHEST_PRECEDENCE)
public class ApiExceptionHandler {
    @ExceptionHandler(MalformedJwtException.class)
    public ResponseEntity<ErrorResponse> handleJwtError() {
        System.out.println("Malformed jwt");
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.FORBIDDEN, "You don`t have jwt for this operation"), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(UserException.class)
    public ResponseEntity<ErrorResponse> handleUserException(UserException userException) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setMessage(userException.getMessage());
        if (userException.status != null) {
            errorResponse.setStatus(userException.status);
            return new ResponseEntity<>(errorResponse, errorResponse.getStatus());
        }
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ErrorResponse> handleCertificateException(ApiException certificateException) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setMessage(certificateException.getMessage());
        if (certificateException.status != null) {
            errorResponse.setStatus(certificateException.status);
            return new ResponseEntity<>(errorResponse, errorResponse.getStatus());
        }
        errorResponse.setStatus(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<ErrorResponse> handleNotFound() {
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.NOT_FOUND, "page NOT found"), (HttpStatus.NOT_FOUND));
    }

    @ExceptionHandler(HibernateException.class)
    public ResponseEntity<ErrorResponse> handleHibernateError() {
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.CONFLICT, "Hibernate error"), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(IllegalStateException.class)
    public ResponseEntity<ErrorResponse> handleIllegalState() {
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.BAD_REQUEST, "Illegal state exception"), HttpStatus.BAD_REQUEST);
    }

}
