package com.epam.esm.exception;

import lombok.extern.log4j.Log4j2;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Log4j2
@Order(value = Ordered.LOWEST_PRECEDENCE)
public class ApiUncaughtExceptionHandler {
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception exception) {
        log.error("Uncaught error : " + exception);
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.BAD_REQUEST, "Uncaught Exception"), (HttpStatus.BAD_REQUEST));
    }
}
