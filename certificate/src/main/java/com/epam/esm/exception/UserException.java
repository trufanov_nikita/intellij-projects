package com.epam.esm.exception;

import org.springframework.http.HttpStatus;

public class UserException extends ApiException {
    public HttpStatus status;

    public UserException(String errorMessage) {
        super(errorMessage);
    }

    public UserException(String errorMessage, HttpStatus status) {
        super("User Exception" + errorMessage, status);
    }

    public UserException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
