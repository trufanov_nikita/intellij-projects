package com.epam.esm.exception;

import org.springframework.http.HttpStatus;

public class ApiException extends Exception {
    public HttpStatus status;

    public ApiException(String errorMessage) {
        super(errorMessage);
    }

    public ApiException(String errorMessage, HttpStatus status) {
        super(errorMessage);
        this.status = status;
    }

    public ApiException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
