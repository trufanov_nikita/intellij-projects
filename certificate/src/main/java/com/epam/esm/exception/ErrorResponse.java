package com.epam.esm.exception;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class ErrorResponse {
    private HttpStatus status;

    private String message;

    public ErrorResponse() {
        this.status = HttpStatus.BAD_REQUEST;
        this.message = "default error response";
    }

    public ErrorResponse(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }
}
