package com.epam.esm.config;

import static com.epam.esm.entity.Role.ADMIN;
import static com.epam.esm.entity.Role.CLIENT;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableMethodSecurity
public class SecurityConfiguration {
    private final JwtAuthenticationFilter jwtAuthFilter;

    private final AuthenticationProvider authenticationProvider;

    private final LogoutHandler logoutHandler;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf()
                .disable()
                .authorizeHttpRequests()
                .requestMatchers("/auth/**")
                .permitAll()
                .requestMatchers(GET, "/hello").permitAll()
                .requestMatchers(GET, "/certificate/**").permitAll()
                .requestMatchers(GET, "/certificate/pagination/**").permitAll()
                .requestMatchers(POST, "/certificate/pagination/**").permitAll()
                .requestMatchers("").permitAll()
                .requestMatchers("/manager/text").permitAll()
                .requestMatchers(DELETE, "/**").hasRole(ADMIN.name())
                .requestMatchers(PUT, "/**").hasRole(ADMIN.name())
                .requestMatchers(POST, "/certificate/**").hasRole(ADMIN.name())
                .requestMatchers(POST, "/user/**").hasRole(ADMIN.name())
                .requestMatchers(POST, "/tag/**").hasRole(ADMIN.name())
                .requestMatchers(POST, "/order/**").hasAnyRole(ADMIN.name(), CLIENT.name())
                .requestMatchers(DELETE, "/order/**").hasRole(ADMIN.name())
                .requestMatchers(GET, "/tag").permitAll()
                .requestMatchers(GET, "/tag/**").hasAnyRole(ADMIN.name(), CLIENT.name())
                .requestMatchers(GET, "/order/**").hasAnyRole(ADMIN.name(), CLIENT.name())
                .requestMatchers("/admin/**").hasRole(ADMIN.name())
                .anyRequest()
                .permitAll()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)
                .logout()
                .logoutUrl("/auth/logout")
                .addLogoutHandler(logoutHandler)
                .logoutSuccessHandler((request, response, authentication) -> SecurityContextHolder.clearContext());
        return http.build();
    }
}
