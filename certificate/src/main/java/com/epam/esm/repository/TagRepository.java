package com.epam.esm.repository;

import com.epam.esm.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    public String mostUsedTagOfUser = "SELECT gift_certificate_tags.tag_id, " +
            "Count(tag_id) AS counter " +
            "FROM user_certificate " +
            "INNER JOIN gift_certificate_tags ON " +
            "gift_certificate_tags.certificate_id = user_certificate.certificate_id " +
            "WHERE user_id = ? " +
            "GROUP BY tag_id " +
            "ORDER BY counter DESC " +
            "LIMIT 1;";

    public void deleteById(long id);

    public boolean existsById(long id);

    public boolean existsByName(String name);

    public List<Tag> findByName(String name);

    @Query(value = mostUsedTagOfUser, nativeQuery = true)
    public Long findMostUsedTagIdByUserId(Long userId);
}
