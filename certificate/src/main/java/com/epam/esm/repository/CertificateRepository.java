package com.epam.esm.repository;

import com.epam.esm.entity.GiftCertificate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface CertificateRepository extends JpaRepository<GiftCertificate, Long> {
    public String findCertificatesWhichContainsListOfTagNames = "SELECT certificate_id, tag_id, tag.name " +
            "FROM certificate.gift_certificate_tags " +
            "join tag on gift_certificate_tags.tag_id = tag.id " +
            "where name in :names " +
            "group by certificate_id " +
            "having count(distinct name) = :count";
    public String findGiftCertificatesWhichContainsListOfTagNames =
            "SELECT gc.id, gc.created_by, gc.created_date, gc.last_modified_by, gc.last_modified_date, gc.create_date, gc.description, gc.duration, gc.last_update_date, gc.name, gc.price  FROM certificate.gift_certificate gc " +
                    "JOIN certificate.gift_certificate_tags gct ON gc.id = gct.certificate_id " +
                    "JOIN certificate.tag t ON gct.tag_id = t.id " +
                    "WHERE t.name IN (:names) " +
                    "GROUP BY gc.id " +
                    "HAVING COUNT(DISTINCT t.name) = :count ";
    public String findGiftCertificatesWhichContainsListOfTagNamesAndName =
            "SELECT gc.id, gc.created_by, gc.created_date, gc.last_modified_by, gc.last_modified_date, gc.create_date, gc.description, gc.duration, gc.last_update_date, gc.name, gc.price FROM certificate.gift_certificate gc " +
                    "JOIN certificate.gift_certificate_tags gct ON gc.id = gct.certificate_id " +
                    "JOIN certificate.tag t ON gct.tag_id = t.id " +
                    "WHERE t.name IN (:names) AND gc.name LIKE CONCAT('%', :certificateName, '%') " +
                    "GROUP BY gc.id " +
                    "HAVING COUNT(DISTINCT t.name) = :count ";

    public boolean existsById(long id);

    public void deleteById(long id);

    public Optional<GiftCertificate> findByNameAndDescriptionAndPriceAndDuration(String name, String description, float price, int duration);

    public List<GiftCertificate> findByTags_Name(String name);

    public List<GiftCertificate> findByTags_Id(Long id);

    public List<GiftCertificate> findByName(String name);

    public List<GiftCertificate> findByNameLike(String name);

    public Page<GiftCertificate> findByNameLike(String name, Pageable pageable);

    public List<GiftCertificate> findByDescription(String description);

    public List<GiftCertificate> findByDescriptionLike(String description);

    public List<GiftCertificate> findAllByOrderByNameAsc();

    public List<GiftCertificate> findAllByOrderByNameDesc();

    public List<GiftCertificate> findAllByOrderByCreateDateAsc();

    public List<GiftCertificate> findAllByOrderByCreateDateDesc();

    public List<GiftCertificate> findAllByOrderByNameAsc(Pageable pageable);

    public List<GiftCertificate> findAllByOrderByNameDesc(Pageable pageable);

    public List<GiftCertificate> findAllByOrderByCreateDateAsc(Pageable pageable);

    public List<GiftCertificate> findAllByOrderByCreateDateDesc(Pageable pageable);

    @Query(value = findCertificatesWhichContainsListOfTagNames, nativeQuery = true)
    public List<GiftCertificate> findByTags(@Param("names") Set<String> names, @Param("count") int count);

    @Query(value = findGiftCertificatesWhichContainsListOfTagNames, nativeQuery = true)
    public Page<GiftCertificate> findByTags(@Param("names") Set<String> names, @Param("count") int count, Pageable pageable);

    @Query(value = findGiftCertificatesWhichContainsListOfTagNamesAndName, nativeQuery = true)
    public Page<GiftCertificate> findByTags(@Param("names") Set<String> names, @Param("count") int count, @Param("certificateName") String certificateName, Pageable pageable);
}
