package com.epam.esm.repository;

import com.epam.esm.entity.Order;
import com.epam.esm.entity.OrderKey;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, OrderKey> {
    public String findRichestUser = "SELECT user_id, MAX(sum) FROM " +
            "(SELECT user_id, SUM(price) as sum " +
            "FROM certificate.user_certificate " +
            "group by user_id order by sum DESC) AS S;";

    @Query(value = findRichestUser, nativeQuery = true)
    List<Object[]> findUserIdWithBiggestSumOfOrders();

    List<Order> findByUserId(Long id, Pageable page);

    void deleteByUserIdAndCertificateId(Long userId, Long certificateId);

    void deleteByCertificateId(Long certificateId);
}
