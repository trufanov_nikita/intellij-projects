package com.epam.esm.controller;

import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.entity.User;
import com.epam.esm.exception.ApiException;
import com.epam.esm.exception.UserException;
import com.epam.esm.service.CertificateService;
import com.epam.esm.service.OrderService;
import com.epam.esm.service.TagService;
import com.epam.esm.service.UserService;

import java.security.SecureRandom;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/createRandomEntities")
public class CreateRandomEntities {
    @Autowired
    private CertificateService certificateService;

    @Autowired
    private TagService tagService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserService userService;

    private SecureRandom rand = new SecureRandom();

    @PutMapping
    public void createRandomEntities() throws UserException {
        for (int i = 1; i < 1001; i++) {
            tagService.addTag(new Tag("tag" + i));
        }
        for (int i = 1; i < 1001; i++) {
            Set<Tag> tags = new HashSet<>();
            for (int j = 1; j < rand.nextInt(10); j++) {
                tags.add(new Tag(rand.nextLong(i), "name"));
            }
            certificateService.addCertificate(new GiftCertificate("certificate" + i, "description of certificate " + i, rand.nextFloat(20), rand.nextInt(100), tags));
        }
        for (int i = 1; i < 1001; i++) {
            userService.create(new User("name of " + i, "surname " + i, "email@gmail.com"));
        }
    }

    @PutMapping("orders")
    public void createRandomOrders() throws ApiException {
        for (int i = 0; i < 1000; i++) {
            userService.order(rand.nextLong(52, 800), rand.nextLong(500));
        }
    }
}
