package com.epam.esm.controller;

import com.epam.esm.entity.PageRequestData;
import com.epam.esm.entity.SortDirection;
import com.epam.esm.service.PaginationCertificateService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@Log4j2
@RestController
@RequestMapping("/certificate/pagination")
public class PaginationCertificateController {
    @Autowired
    private PaginationCertificateService certificateService;

    @GetMapping
    public ResponseEntity<Object> getAll(@RequestParam("page") int page) {
        return certificateService.getCertificates(page);
    }

    @GetMapping("/modified")
    public ResponseEntity<Object> getAllModified(@RequestParam int page, @RequestParam int pageSize, @RequestParam String sortProperty, @RequestParam SortDirection sortDirection) {
        PageRequestData pageRequest = new PageRequestData(page, pageSize, sortProperty.trim(), sortDirection);
        return certificateService.getCertificatesModified(pageRequest);
    }

    @GetMapping("/tag")
    public ResponseEntity<Object> findByTags(@RequestParam("tags") Set<String> names, @RequestParam int page, @RequestParam int pageSize, @RequestParam String sortProperty, @RequestParam SortDirection sortDirection) {
        PageRequestData pageRequest = new PageRequestData(page, pageSize, sortProperty.trim(), sortDirection);
        return certificateService.findByTags(names, pageRequest);
    }

    @GetMapping("/search/tag/{name}")
    public ResponseEntity<Object> findByTagsAndCertificateName(@PathVariable String name, @RequestParam("tags") Set<String> names, @RequestParam int page, @RequestParam int pageSize, @RequestParam String sortProperty, @RequestParam SortDirection sortDirection) {

        PageRequestData pageRequest = new PageRequestData(page, pageSize, sortProperty.trim(), sortDirection);
        log.info("Search with tags " + names + " " + pageRequest);
        log.info("Search with tags result" + certificateService.findByTags(names, name, pageRequest));
        return certificateService.findByTags(names, name, pageRequest);
    }

    @GetMapping("/search/{name}")
    public ResponseEntity<Object> getByNameLike(@PathVariable String name, @RequestParam int page, @RequestParam int pageSize, @RequestParam String sortProperty, @RequestParam SortDirection sortDirection) {
        PageRequestData pageRequest = new PageRequestData(page, pageSize, sortProperty.trim(), sortDirection);
        return certificateService.findByNameLike(name, pageRequest);
    }

    @GetMapping("/name/Asc")
    public ResponseEntity<Object> getByNameAsc(@RequestParam("page") int page) {
        return certificateService.findAllOrderByNameAsc(page);
    }

    @GetMapping("/name/Desc")
    public ResponseEntity<Object> getByNameDesc(@RequestParam("page") int page) {
        return certificateService.findAllOrderByNameDesc(page);
    }

    @GetMapping("/date/Asc")
    public ResponseEntity<Object> getByDateAsc(@RequestParam("page") int page) {
        return certificateService.findAllOrderByDateAsc(page);
    }

    @GetMapping("/date/Desc")
    public ResponseEntity<Object> getByDateDesc(@RequestParam("page") int page) {
        return certificateService.findAllOrderByDateDesc(page);
    }
}
