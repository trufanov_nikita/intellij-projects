package com.epam.esm.controller;

import com.epam.esm.entity.Tag;
import com.epam.esm.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tag")
public class TagController {
    @Autowired
    private TagService tagService;

    @GetMapping
    public Iterable<Tag> getAll() {
        return tagService.getTags();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getById(@PathVariable Long id) {
        return tagService.getTag(id);
    }

    @PostMapping
    public ResponseEntity<Object> add(@RequestBody Tag tag) {
        return tagService.addTag(tag);
    }

    @PostMapping("/{id}")
    public ResponseEntity<Object> edit(@PathVariable Long id, @RequestBody Tag tag) {
        return tagService.editTag(id, tag);
    }

    @Deprecated
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable Long id) {
        return tagService.deleteTag(id);
    }
}
