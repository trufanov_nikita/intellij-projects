package com.epam.esm.controller;

import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.service.CertificateService;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/certificate")
public class CertificateController {
    @Autowired
    private CertificateService certificateService;

    @GetMapping
    public Iterable<GiftCertificate> getAll() {
        return certificateService.getCertificates();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getById(@PathVariable Long id) {
        return certificateService.getCertificate(id);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<Object> getByName(@PathVariable String name) {
        return certificateService.findByName(name);
    }

    @GetMapping("/name/Asc")
    public List<GiftCertificate> getByNameAsc() {
        return certificateService.findAllOrderByNameAsc();
    }

    @GetMapping("/name/Desc")
    public List<GiftCertificate> getByNameDesc() {
        return certificateService.findAllOrderByNameDesc();
    }

    @GetMapping("/date/Asc")
    public List<GiftCertificate> getByDateAsc() {
        return certificateService.findAllOrderByDateAsc();
    }

    @GetMapping("/date/Desc")
    public List<GiftCertificate> getByDateDesc() {
        return certificateService.findAllOrderByDateDesc();
    }

    @GetMapping("/name/{name}/like")
    public ResponseEntity<Object> getByNameLike(@PathVariable String name) {
        return certificateService.findByNameLike(name);
    }

    @GetMapping("/description/{name}")
    public ResponseEntity<Object> getByDescription(@PathVariable String name) {
        return certificateService.findByDescription(name);
    }

    @GetMapping("/description/{name}/like")
    public ResponseEntity<Object> getByDescriptionLike(@PathVariable String name) {
        return certificateService.findByDescriptionLike(name);
    }

    @GetMapping("/tag/{name}")
    public ResponseEntity<Object> findByTagName(@PathVariable String name) {
        return certificateService.findByTagName(name);
    }

    @GetMapping("/tag/names")
    public List<GiftCertificate> findByTags(@RequestBody Set<String> names) {
        return certificateService.findByTags(names);
    }

    @PostMapping
    public ResponseEntity<Object> add(@RequestBody @Validated GiftCertificate giftCertificate) {
        return certificateService.addCertificate(giftCertificate);
    }

    @PostMapping("/{id}")
    public ResponseEntity<Object> edit(@PathVariable Long id, @RequestBody GiftCertificate certificate) {
        certificate.setId(id);
        return certificateService.editCertificate(id, certificate);
    }

    @DeleteMapping("tag/{id}")
    public ResponseEntity<Object> deleteTag(@PathVariable Long id) {
        return certificateService.deleteTagInAllCertificates(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable Long id) {
        certificateService.deleteCertificate(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
