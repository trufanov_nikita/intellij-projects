package com.epam.esm.controller;

import com.epam.esm.exception.ApiException;
import com.epam.esm.service.OrderService;
import com.epam.esm.service.TagService;
import com.epam.esm.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
@Log4j2
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private UserService userService;

    @Autowired
    private TagService tagService;

    @GetMapping("/mostUsedTag")
    public ResponseEntity<Object> findMostUsedTag() throws ApiException {
        return tagService.findMostUsedTagFromUserId(orderService.findUserIdWithBiggestSumOfOrders());
    }

    @GetMapping
    public ResponseEntity<Object> findAll(@RequestParam("page") int page) {
        return orderService.findAll(page);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<Object> findByUserId(@RequestParam("page") int page, @PathVariable Long id) throws ApiException {
        return orderService.findByUserId(id, page);
    }

    @PostMapping("/{userId}/{certificateId}")
    public ResponseEntity<Object> order(@PathVariable Long userId, @PathVariable Long certificateId) throws ApiException {
        log.info("Order from user with id  " + userId + " for certificate with id " + certificateId);
        return userService.order(userId, certificateId);
    }

    @DeleteMapping("/{userId}/{certificateId}")
    public ResponseEntity<Object> delete(@PathVariable Long userId, @PathVariable Long certificateId) {
        return orderService.deleteOrder(userId, certificateId);
    }

    @DeleteMapping("/certificate/{certificateId}")
    public ResponseEntity<Object> delete(@PathVariable Long certificateId) {
        return orderService.deleteCertificateFromOrders(certificateId);
    }
}
