package com.epam.esm.service;

import com.epam.esm.controller.PaginationCertificateController;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.PageRequestData;
import com.epam.esm.exception.ErrorResponse;
import com.epam.esm.repository.CertificateRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Log4j2
@Service
public class PaginationCertificateService extends CertificateService {
    @Autowired
    private CertificateRepository certificateRepository;

    @Autowired
    private TagService tagService;

    private final static String incorrectValues = "Some values was null or incorrect";

    private final static String certificateNotFoundWithName = "Certificate not found with name: ";

    private static final int pageSize = 5;

    private static final String nextLink = "nextPage";

    private static final String prevLink = "prevPage";

    private static final String lastLink = "lastPage";

    public ResponseEntity<Object> getCertificates(int page) {
        PageRequest pageRequest = PageRequest.of(page, pageSize);
        Page<GiftCertificate> list = certificateRepository.findAll(pageRequest);
        list.forEach(x -> x.buildLinks());
        Link selfLink = linkTo(methodOn(PaginationCertificateController.class).getAll(page)).withSelfRel();

        Link lastPage = linkTo(methodOn(PaginationCertificateController.class).getAll(list.getTotalPages())).withRel(lastLink);
        CollectionModel<GiftCertificate> result = CollectionModel.of(list, selfLink, lastPage);

        if (page > 0) {
            Link prevPage = linkTo(methodOn(PaginationCertificateController.class).getAll(page - 1)).withRel(prevLink);
            result.add(prevPage);
        }
        if (page < list.getTotalPages() - 1) {
            Link nextPage = linkTo(methodOn(PaginationCertificateController.class).getAll(page + 1)).withRel(nextLink);
            result.add(nextPage);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Object> getCertificatesModified(PageRequestData pageRequestData) {
        PageRequest pageRequest = getPageRequest(pageRequestData);
        int page = pageRequestData.getPage();
        log.info("get certificates Modified " + pageRequest);
        Page<GiftCertificate> list = certificateRepository.findAll(pageRequest);
        list.forEach(x -> x.buildLinks());
        Link selfLink = linkTo(methodOn(PaginationCertificateController.class).getAllModified(pageRequest.getPageNumber(), pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withSelfRel();

        Link lastPage = linkTo(methodOn(PaginationCertificateController.class).getAllModified(list.getTotalPages(), pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withRel(lastLink);
        CollectionModel<GiftCertificate> result = CollectionModel.of(list, selfLink, lastPage);

        if (page > 0) {
            Link prevPage = linkTo(methodOn(PaginationCertificateController.class).getAllModified(page - 1, pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withRel(prevLink);
            result.add(prevPage);
        }
        if (page < list.getTotalPages() - 1) {
            Link nextPage = linkTo(methodOn(PaginationCertificateController.class).getAllModified(page + 1, pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withRel(nextLink);
            result.add(nextPage);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Object> findByTags(Set<String> tags, PageRequestData pageRequestData) {
        PageRequest pageRequest = getPageRequestForNative(pageRequestData);
        int page = pageRequestData.getPage();
        log.info("find by tags without name  " + pageRequest);
        Page<GiftCertificate> list = certificateRepository.findByTags(tags, tags.size(), pageRequest);
        list.forEach(x -> x.buildLinks());
        Link selfLink = linkTo(methodOn(PaginationCertificateController.class).findByTags(tags, pageRequest.getPageNumber(), pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withSelfRel();

        Link lastPage = linkTo(methodOn(PaginationCertificateController.class).findByTags(tags, list.getTotalPages(), pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withRel(lastLink);
        CollectionModel<GiftCertificate> result = CollectionModel.of(list, selfLink, lastPage);

        if (page > 0) {
            Link prevPage = linkTo(methodOn(PaginationCertificateController.class).findByTags(tags, page - 1, pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withRel(prevLink);
            result.add(prevPage);
        }
        if (page < list.getTotalPages() - 1) {
            Link nextPage = linkTo(methodOn(PaginationCertificateController.class).findByTags(tags, page + 1, pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withRel(nextLink);
            result.add(nextPage);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Object> findByTags(Set<String> tags, String name, PageRequestData pageRequestData) {
        PageRequest pageRequest = getPageRequestForNative(pageRequestData);
        int page = pageRequestData.getPage();
        log.info("find by tags with name " + name + tags + tags.size() + pageRequest);
        Page<GiftCertificate> list = certificateRepository.findByTags(tags, tags.size(), name, pageRequest);
        list.forEach(x -> x.buildLinks());
        Link selfLink = linkTo(methodOn(PaginationCertificateController.class).findByTagsAndCertificateName(name, tags, pageRequest.getPageNumber(), pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withSelfRel();

        Link lastPage = linkTo(methodOn(PaginationCertificateController.class).findByTagsAndCertificateName(name, tags, list.getTotalPages(), pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withRel(lastLink);
        CollectionModel<GiftCertificate> result = CollectionModel.of(list, selfLink, lastPage);

        if (page > 0) {
            Link prevPage = linkTo(methodOn(PaginationCertificateController.class).findByTagsAndCertificateName(name, tags, page - 1, pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withRel(prevLink);
            result.add(prevPage);
        }
        if (page < list.getTotalPages() - 1) {
            Link nextPage = linkTo(methodOn(PaginationCertificateController.class).findByTagsAndCertificateName(name, tags, page + 1, pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withRel(nextLink);
            result.add(nextPage);
        }
        Map<String, Object> response = new HashMap<>();
        response.put("totalPages", list.getTotalPages());
        response.put("data", result);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    public ResponseEntity<Object> findByNameLike(String name, PageRequestData pageRequestData) {
        PageRequest pageRequest = getPageRequest(pageRequestData);
        log.info("find by name like " + pageRequest);
        if (name == null || name.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(HttpStatus.BAD_REQUEST, incorrectValues));
        }
        Page<GiftCertificate> list = certificateRepository.findByNameLike("%" + name + "%", pageRequest);
        if (list == null || list.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, certificateNotFoundWithName + name));
        }
        Link lastPage = linkTo(methodOn(PaginationCertificateController.class).getByNameLike(name, list.getTotalPages(), pageRequest.getPageSize(), pageRequestData.getSortProperty(), pageRequestData.getSortDirection())).withRel(lastLink);
        CollectionModel<GiftCertificate> result = CollectionModel.of(list, lastPage);
        list.forEach(x -> x.buildLinks());
        Map<String, Object> response = new HashMap<>();
        response.put("totalPages", list.getTotalPages());
        response.put("data", result);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    public ResponseEntity<Object> findAllOrderByNameAsc(int page) {
        PageRequest pageRequest = PageRequest.of(page, pageSize);
        List<GiftCertificate> list = certificateRepository.findAllByOrderByNameAsc(pageRequest);
        list.forEach(x -> x.buildLinks());
        Link selfLink = linkTo(methodOn(PaginationCertificateController.class).getByNameAsc(page)).withSelfRel();
        Link nextPage = linkTo(methodOn(PaginationCertificateController.class).getByNameAsc(page + 1)).withRel(nextLink);
        CollectionModel<GiftCertificate> result = CollectionModel.of(list, selfLink);
        result.add(nextPage);
        if (page > 0) {
            Link prevPage = linkTo(methodOn(PaginationCertificateController.class).getByNameAsc(page - 1)).withRel(prevLink);
            result.add(prevPage);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Object> findAllOrderByNameDesc(int page) {
        PageRequest pageRequest = PageRequest.of(page, pageSize);
        List<GiftCertificate> list = certificateRepository.findAllByOrderByNameDesc(pageRequest);
        list.forEach(x -> x.buildLinks());
        Link selfLink = linkTo(methodOn(PaginationCertificateController.class).getByNameDesc(page)).withSelfRel();
        Link nextPage = linkTo(methodOn(PaginationCertificateController.class).getByNameDesc(page + 1)).withRel(nextLink);
        CollectionModel<GiftCertificate> result = CollectionModel.of(list, selfLink);
        result.add(nextPage);
        if (page > 0) {
            Link prevPage = linkTo(methodOn(PaginationCertificateController.class).getByNameDesc(page - 1)).withRel(prevLink);
            result.add(prevPage);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Object> findAllOrderByDateAsc(int page) {
        PageRequest pageRequest = PageRequest.of(page, pageSize);
        List<GiftCertificate> list = certificateRepository.findAllByOrderByCreateDateAsc(pageRequest);
        list.forEach(x -> x.buildLinks());
        Link selfLink = linkTo(methodOn(PaginationCertificateController.class).getByDateAsc(page)).withSelfRel();
        Link nextPage = linkTo(methodOn(PaginationCertificateController.class).getByDateAsc(page + 1)).withRel(nextLink);
        CollectionModel<GiftCertificate> result = CollectionModel.of(list, selfLink);
        result.add(nextPage);
        if (page > 0) {
            Link prevPage = linkTo(methodOn(PaginationCertificateController.class).getByDateAsc(page - 1)).withRel(prevLink);
            result.add(prevPage);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Object> findAllOrderByDateDesc(int page) {
        PageRequest pageRequest = PageRequest.of(page, pageSize);
        List<GiftCertificate> list = certificateRepository.findAllByOrderByCreateDateDesc(pageRequest);
        list.forEach(x -> x.buildLinks());
        Link selfLink = linkTo(methodOn(PaginationCertificateController.class).getByDateDesc(page)).withSelfRel();
        Link nextPage = linkTo(methodOn(PaginationCertificateController.class).getByDateDesc(page + 1)).withRel(nextLink);
        CollectionModel<GiftCertificate> result = CollectionModel.of(list, selfLink);
        result.add(nextPage);
        if (page > 0) {
            Link prevPage = linkTo(methodOn(PaginationCertificateController.class).getByDateDesc(page - 1)).withRel(prevLink);
            result.add(prevPage);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    private static PageRequest getPageRequest(PageRequestData pageRequestData) {
        PageRequest pageRequest;
        switch (pageRequestData.getSortDirection()) {
            case ASC: {
                pageRequest = PageRequest.of(pageRequestData.getPage(), pageRequestData.getPageSize()).withSort(Sort.by(pageRequestData.getSortProperty()).ascending());
                break;
            }
            case DESC: {
                pageRequest = PageRequest.of(pageRequestData.getPage(), pageRequestData.getPageSize()).withSort(Sort.by(pageRequestData.getSortProperty()).descending());
                break;
            }
            default: {
                pageRequest = PageRequest.of(pageRequestData.getPage(), pageRequestData.getPageSize()).withSort(Sort.by(pageRequestData.getSortProperty()));
                break;
            }
        }
        return pageRequest;
    }

    private static PageRequest getPageRequestForNative(PageRequestData pageRequestData) {
        switch (pageRequestData.getSortProperty()) {
            case "createdDate": {
                pageRequestData.setSortProperty("created_date");
                break;
            }
            case "lastModifiedDate": {
                pageRequestData.setSortProperty("last_modified_date");
            }
        }
        return getPageRequest(pageRequestData);
    }
}
