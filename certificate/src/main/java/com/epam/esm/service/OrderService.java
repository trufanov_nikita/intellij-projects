package com.epam.esm.service;

import com.epam.esm.controller.OrderController;
import com.epam.esm.entity.Order;
import com.epam.esm.exception.ApiException;
import com.epam.esm.repository.OrderRepository;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@Log4j2
@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    private static final String nextLink = "nextPage";

    private static final String prevLink = "prevPage";

    private static final int pageSize = 5;

    public Long findUserIdWithBiggestSumOfOrders() throws ApiException {
        List<Object[]> objects = orderRepository.findUserIdWithBiggestSumOfOrders();
        if (objects.size() == 0) {
            throw new ApiException("No orders yet to calculate", HttpStatus.BAD_REQUEST);
        }
        Object[] userIdAndSumOfOrders = objects.get(0);
        log.info("User id with biggest sum of orders: " + userIdAndSumOfOrders[0] + " Sum of orders: " + userIdAndSumOfOrders[1]);
        return (Long) userIdAndSumOfOrders[0];
    }

    public ResponseEntity<Object> findAll(int page) {
        PageRequest pageRequest = PageRequest.of(page, pageSize);
        pageRequest.withSort(Sort.by("purchase_date"));
        Page<Order> list = orderRepository.findAll(pageRequest);
        Link selfLink = linkTo(methodOn(OrderController.class).findAll(page)).withSelfRel();
        list.forEach(x -> x.buildLinks());
        Link nextPage = linkTo(methodOn(OrderController.class).findAll(page + 1)).withRel(nextLink);
        CollectionModel<Order> result = CollectionModel.of(list, selfLink, nextPage);
        if (page > 0) {
            Link prevPage = linkTo(methodOn(OrderController.class).findAll(page - 1)).withRel(prevLink);
            result.add(prevPage);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Object> findByUserId(Long id, int page) throws ApiException {
        PageRequest pageRequest = PageRequest.of(page, pageSize);
        List<Order> list = orderRepository.findByUserId(id, pageRequest);
        Link selfLink = linkTo(methodOn(OrderController.class).findByUserId(page, id)).withSelfRel();
        list.forEach(x -> x.buildLinks());
        Link nextPage = linkTo(methodOn(OrderController.class).findByUserId(page + 1, id)).withRel(nextLink);
        CollectionModel<Order> result = CollectionModel.of(list, selfLink, nextPage);
        if (page > 0) {
            Link prevPage = linkTo(methodOn(OrderController.class).findByUserId(page - 1, id)).withRel(prevLink);
            result.add(prevPage);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Transactional
    public ResponseEntity<Object> deleteOrder(Long userId, Long certificateId) {
        orderRepository.deleteByUserIdAndCertificateId(userId, certificateId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Transactional
    public ResponseEntity<Object> deleteCertificateFromOrders(Long certificateId) {
        orderRepository.deleteByCertificateId(certificateId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
