package com.epam.esm.service;

import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Order;
import com.epam.esm.entity.OrderKey;
import com.epam.esm.entity.User;
import com.epam.esm.exception.ApiException;
import com.epam.esm.exception.UserException;
import com.epam.esm.repository.OrderRepository;
import com.epam.esm.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service
public class UserService  {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CertificateService certificateService;

    @Autowired
    private OrderRepository orderRepository;

    private static final String noUser = "No user with this id: ";

    private static final String emailBusy = "Email is busy";

    private static final String someFieldsIncorrect = "Some fields are incorrect";

    private static final String certificateNotFound = "Certificate Not found";

    private static final String purchaseCompleted = "Purchase completed";

    public ResponseEntity<Object> findAll(){
        return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
    }

    public ResponseEntity<Object> findById(Long id) throws UserException{
        if(userRepository.existsById(id)){
            User user = userRepository.findById(id).get();
            user.buildLinks();
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
        throw new UserException(noUser + id);
    }
    @Transactional
    public ResponseEntity<Object> create(User user) throws UserException {
        if(validateFields(user)){
            if(userRepository.findByEmail(user.getEmail()).isPresent())
                throw new UserException(emailBusy);
            User result = userRepository.save(user);
            result.buildLinks();
            return new ResponseEntity<>(result, HttpStatus.CREATED);
        }
        throw new UserException(someFieldsIncorrect);
    }

    @Transactional
    public ResponseEntity<Object> order(Long userId, Long certificateId) throws UserException, ApiException {
        if(!userRepository.existsById(userId)){
            throw new UserException(noUser + userId);
        }
        ResponseEntity<Object> certificateResponse = certificateService.getCertificate(certificateId);
        if(certificateResponse.getStatusCode() == HttpStatus.NOT_FOUND){
            throw new ApiException(certificateNotFound);
        }
        OrderKey id = new OrderKey(userId, certificateId);
        if(orderRepository.existsById(id)){
            throw new ApiException("This order already exists", HttpStatus.CONFLICT);
        }
        GiftCertificate certificate = (GiftCertificate) certificateResponse.getBody();
        User user = userRepository.findById(userId).get();
        OrderKey orderKey = new OrderKey(userId, certificateId);
        Order order = new Order(orderKey, user, certificate, certificate.getPrice(), ZonedDateTime.now());
        user.addOrder(order);
        userRepository.save(user);
        return new ResponseEntity<>(purchaseCompleted, HttpStatus.OK);
    }

    private boolean validateFields(User user){
        if(user.getName() == null || user.getName().isEmpty() || user.getSurname() == null || user.getSurname().isEmpty()){
            return false;
        }
        return user.getEmail() != null && user.getEmail().matches("^(.+)@(\\S+)$");
    }
}
