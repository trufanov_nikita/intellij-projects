package com.epam.esm.service;

import com.epam.esm.entity.Tag;
import com.epam.esm.exception.ErrorResponse;
import com.epam.esm.repository.CertificateRepository;
import com.epam.esm.repository.TagRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService {
    @Autowired
    private CertificateRepository certificateRepository;

    @Autowired
    private TagRepository tagRepository;

    private static final String notFound = "No tag with id :";

    private static final String notFoundByName = "No tag with name: ";

    private static final String badData = "Name is null or empty";

    private static final String alreadyExists = "Tag already exists with this id:";

    private static final String alreadyExistsWithName = "Tag already exists with this name: ";

    @Transactional
    public ResponseEntity<Object> addTag(Tag tag) {
        if(tag.getName() == null || tag.getName().isEmpty()){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse(HttpStatus.BAD_REQUEST, badData));
        }
        if (tag.getId() != null && tagRepository.existsById(tag.getId())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse(HttpStatus.CONFLICT, alreadyExists + tag.getId()));
        }
        if (tagRepository.existsByName(tag.getName())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse(HttpStatus.CONFLICT, alreadyExistsWithName + tag.getName()));
        }
        Tag result = tagRepository.save(tag);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public List<Tag> getTags() {
        var list = tagRepository.findAll();
        list.forEach(x -> x.buildLinks());
        return list;
    }

    public ResponseEntity<Object> getTag(Long id) {
        if (tagRepository.existsById(id)) {
            Tag tag = tagRepository.findById(id).get();
            tag.buildLinks();
            return new ResponseEntity<>(tag, HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, notFound + id));
    }

    public ResponseEntity<Object> getTag(String name) {
        Tag tag = tagRepository.findByName(name).stream().findFirst().get();
        if (tag != null) {
            tag.buildLinks();
            return new ResponseEntity<>(tag, HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, notFoundByName + name));
    }

    public ResponseEntity<Object> findMostUsedTagFromUserId(Long id) {
        return getTag(tagRepository.findMostUsedTagIdByUserId(id));
    }

    public ResponseEntity<Object> editTag(Long id, Tag tag) {
        if (tagRepository.existsById(id)) {
            tag.setId(id);
            tag.buildLinks();
            return new ResponseEntity<>(tagRepository.save(tag), HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, notFound + id));
    }

    public ResponseEntity<Object> deleteTag(Long id) {
        if (tagRepository.existsById(id)) {
            tagRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, notFound + id));
    }
}
