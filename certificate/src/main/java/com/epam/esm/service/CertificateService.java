package com.epam.esm.service;

import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.exception.ErrorResponse;
import com.epam.esm.repository.CertificateRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CertificateService {
    @Autowired
    private CertificateRepository certificateRepository;

    @Autowired
    private TagService tagService;

    @Autowired
    private OrderService orderService;

    private static final String alreadyExists = "GiftCertificate already exists with id :";

    private static final String incorrectValues = "Some values was null or incorrect";

    private static final String tagNotFound = "Not tag with id :";

    private static final String certificateNotFound = "GiftCertificate not found with id: ";

    private static final String certificateNotFoundWithDesc = "Certificate not found with description: ";

    private static final String certificateNotFoundWithName = "Certificate not found with name: ";

    private static final String getCertificateNotFoundWithTags = "GiftCertificate not found with tags that has name ";

    @Transactional
    public ResponseEntity<Object> addCertificate(GiftCertificate certificate) {
        if (certificate.getId() != null && certificateRepository.existsById(certificate.getId())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse(HttpStatus.CONFLICT, alreadyExists + certificate.getId()));
        }

        if (certificate.getName() == null || certificate.getDescription() == null || certificate.getDuration() == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(HttpStatus.BAD_REQUEST, incorrectValues));
        }
        if (certificate.getCreateDate() == null) {
            certificate.setCreateDate(ZonedDateTime.now(ZoneOffset.UTC));
        }
        if (certificate.getLastUpdateDate() == null) {
            certificate.setLastUpdateDate(ZonedDateTime.now(ZoneOffset.UTC));
        }
        Set<Tag> tags = certificate.getTags();
        if (tags != null && !tags.isEmpty()) {
            for (Tag tag : tags) {
                if (tag.getId() != null) {
                    ResponseEntity response = tagService.getTag(tag.getId());
                    if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
                        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, tagNotFound + tag.getId()));
                    }
                    Tag temp = (Tag) response.getBody();
                    tag.setName(temp.getName());
                } else {
                    ResponseEntity response = tagService.getTag(tag.getName());
                    if(response.getStatusCode() == HttpStatus.NOT_FOUND){
                        tagService.addTag(tag);
                    }
                    else if(response.getStatusCode() == HttpStatus.OK){
                        Tag foundTag = (Tag) response.getBody();
                        Set<Tag> temp = certificate.getTags().stream().map(x -> {
                            if(x.getName() == tag.getName()) {
                                x.setId(foundTag.getId());
                            }
                            return x;
                        }).collect(Collectors.toSet());
                        certificate.setTags(temp);
                    }
                }
            }
        }
        GiftCertificate result = certificateRepository.save(certificate);
        result.buildLinks();
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    public Iterable<GiftCertificate> getCertificates() {
        return buildLinks(certificateRepository.findAll());
    }

    public List<GiftCertificate> findAllOrderByNameAsc() {
        return buildLinks(certificateRepository.findAllByOrderByNameAsc());
    }

    public List<GiftCertificate> findAllOrderByNameDesc() {
        return buildLinks(certificateRepository.findAllByOrderByNameDesc());
    }

    public List<GiftCertificate> findAllOrderByDateAsc() {
        return buildLinks(certificateRepository.findAllByOrderByCreateDateAsc());
    }

    public List<GiftCertificate> findAllOrderByDateDesc() {
        return buildLinks(certificateRepository.findAllByOrderByCreateDateDesc());
    }

    public ResponseEntity<Object> getCertificate(Long id) {
        Optional<GiftCertificate> certificate = certificateRepository.findById(id);
        if (certificate.isPresent()) {
            GiftCertificate result = certificate.get();
            result.buildLinks();
            return new ResponseEntity<>(certificate.get(), HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, certificateNotFound + id));

    }

    @Transactional
    public ResponseEntity<Object> deleteCertificate(Long id) {
        if (certificateRepository.existsById(id)) {
            orderService.deleteCertificateFromOrders(id);
            certificateRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, certificateNotFound + id));
    }

    @Transactional
    public ResponseEntity<Object> editCertificate(Long id, GiftCertificate certificate) {
        if (certificateRepository.existsById(id)) {
            GiftCertificate foundCertificate = (GiftCertificate) getCertificate(id).getBody();
            certificate.setId(id);
            Set<Tag> tags = certificate.getTags();
            if (tags != null && !tags.isEmpty()) {
                for (Tag tag : tags) {
                    if (tag.getId() != null) {
                        ResponseEntity response = tagService.getTag(tag.getId());
                        if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
                            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, tagNotFound + tag.getId()));
                        }
                        Tag temp = (Tag) response.getBody();
                        tag.setName(temp.getName());
                    } else {
                        ResponseEntity response = tagService.getTag(tag.getName());
                        if(response.getStatusCode() == HttpStatus.NOT_FOUND){
                            tagService.addTag(tag);
                        }
                        else if(response.getStatusCode() == HttpStatus.OK){
                            Tag foundTag = (Tag) response.getBody();
                            Set<Tag> temp = certificate.getTags().stream().map(x -> {
                                if(x.getName() == tag.getName()) {
                                    x.setId(foundTag.getId());
                                }
                                return x;
                            }).collect(Collectors.toSet());
                            certificate.setTags(temp);
                        }
                    }
                }
            }
            certificate.buildLinks();
            foundCertificate.buildLinks();

            return new ResponseEntity<>(certificateRepository.save(validateEditData(foundCertificate, certificate)), HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, certificateNotFound + id));
    }

    public ResponseEntity<Object> findByTagName(String name) {
        if (name == null || name.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(HttpStatus.BAD_REQUEST, incorrectValues));
        }
        List<GiftCertificate> list = certificateRepository.findByTags_Name(name.trim());
        if (list == null || list.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, getCertificateNotFoundWithTags + name));
        }
        return new ResponseEntity<>(buildLinks(list), HttpStatus.OK);
    }

    public ResponseEntity<Object> deleteTagInAllCertificates(Long id) {
        ResponseEntity response = tagService.getTag(id);
        if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, tagNotFound + id));
        }
        Tag tag = (Tag) response.getBody();
        certificateRepository.findByTags_Id(tag.getId()).forEach(x -> {
            x.deleteTag(tag);
            editCertificate(x.getId(), x);
        });
        tagService.deleteTag(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Object> findByName(String name) {
        if (name == null || name.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(HttpStatus.BAD_REQUEST, incorrectValues));
        }
        List<GiftCertificate> list = certificateRepository.findByName(name);
        if (list == null || list.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, certificateNotFoundWithName + name));
        }
        return new ResponseEntity<>(buildLinks(list), HttpStatus.OK);
    }

    public ResponseEntity<Object> findByNameLike(String name) {
        if (name == null || name.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(HttpStatus.BAD_REQUEST, incorrectValues));
        }
        List<GiftCertificate> list = certificateRepository.findByNameLike("%" + name + "%");
        if (list == null || list.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, certificateNotFoundWithName + name));
        }
        return new ResponseEntity<>(buildLinks(list), HttpStatus.OK);
    }


    public ResponseEntity<Object> findByDescription(String description) {
        if (description == null || description.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(HttpStatus.BAD_REQUEST, incorrectValues));
        }
        List<GiftCertificate> list = certificateRepository.findByDescription(description);
        if (list == null || list.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, certificateNotFoundWithDesc + description));
        }
        return new ResponseEntity<>(buildLinks(list), HttpStatus.OK);
    }

    public ResponseEntity<Object> findByDescriptionLike(String description) {
        if (description == null || description.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(HttpStatus.BAD_REQUEST, incorrectValues));
        }
        List<GiftCertificate> list = certificateRepository.findByDescriptionLike("%" + description + "%");
        if (list == null || list.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.NOT_FOUND, certificateNotFoundWithDesc + description));
        }
        return new ResponseEntity<>(buildLinks(list), HttpStatus.OK);
    }

    public List<GiftCertificate> findByTags(Set<String> tags) {
        return certificateRepository.findByTags(tags, tags.size());
    }

    private GiftCertificate validateEditData(GiftCertificate original, GiftCertificate toChange) {
        if (toChange.getName() == null) {
            toChange.setName(original.getName());
        }
        if (toChange.getDuration() == 0) {
            toChange.setDuration(original.getDuration());
        }
        if (toChange.getDescription() == null) {
            toChange.setDescription(original.getDescription());
        }
        if (toChange.getTags() == null) {
            toChange.setTags(original.getTags());
        }
        if (toChange.getPrice() == 0) {
            toChange.setPrice(original.getPrice());
        }
        if (toChange.getCreateDate() == null) {
            toChange.setCreateDate(original.getCreateDate());
        }
        if (toChange.getLastUpdateDate() == null) {
            toChange.setLastUpdateDate(ZonedDateTime.now(ZoneOffset.UTC));
        }
        return toChange;
    }

    private List<GiftCertificate> buildLinks(List<GiftCertificate> list) {
        list.forEach(x -> x.buildLinks());
        return list;
    }
}
