package com.epam.esm.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.epam.esm.entity.Permission.ADMIN_CREATE;
import static com.epam.esm.entity.Permission.ADMIN_DELETE;
import static com.epam.esm.entity.Permission.ADMIN_READ;
import static com.epam.esm.entity.Permission.ADMIN_UPDATE;
import static com.epam.esm.entity.Permission.CLIENT_ORDER;
import static com.epam.esm.entity.Permission.CLIENT_READ;

@RequiredArgsConstructor
public enum Role {
    USER(Collections.emptySet()),
    ADMIN(
            Set.of(
                    ADMIN_READ,
                    ADMIN_UPDATE,
                    ADMIN_DELETE,
                    ADMIN_CREATE,
                    CLIENT_READ,
                    CLIENT_ORDER
            )
    ),
    CLIENT(
            Set.of(
                    CLIENT_READ,
                    CLIENT_ORDER
            )
    );

    @Getter
    private final Set<Permission> permissions;

    public List<SimpleGrantedAuthority> getAuthorities() {
        var authorities = getPermissions()
                .stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermissionName()))
                .collect(Collectors.toList());
        authorities.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return authorities;
    }
}
