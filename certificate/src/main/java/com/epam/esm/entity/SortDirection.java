package com.epam.esm.entity;

public enum SortDirection {
    ASC, DESC
}
