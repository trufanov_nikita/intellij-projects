package com.epam.esm.entity;


import com.epam.esm.audit.AuditableAndRepresentation;
import com.epam.esm.controller.CertificateController;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.annotation.Nonnull;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Set;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.TimeZoneStorage;
import org.hibernate.annotations.TimeZoneStorageType;
import org.springframework.hateoas.Link;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Entity
@Table(name = "gift_certificate")
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class GiftCertificate extends AuditableAndRepresentation<String> {
    @Id
    @GeneratedValue
    private Long id;

    @Nonnull
    private String name;

    @Nonnull
    @Column(columnDefinition = "TEXT")
    private String description;

    @Nonnull
    private float price;

    @Nonnull
    private int duration;

    @Nonnull
    @Column(name = "create_date")
    @TimeZoneStorage(TimeZoneStorageType.NATIVE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    private ZonedDateTime createDate;

    @Nonnull
    @Column(name = "last_update_date")
    @TimeZoneStorage(TimeZoneStorageType.NATIVE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    private ZonedDateTime lastUpdateDate;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToMany(fetch = FetchType.EAGER, targetEntity = Tag.class, cascade = CascadeType.MERGE)
    @JoinTable(
            joinColumns = {@JoinColumn(name = "certificate_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id")}
    )
    private Set<Tag> tags;

    @JsonIgnore
    @OneToMany(mappedBy = "certificate")
    private Set<Order> orders;

    public GiftCertificate() {

    }

    public GiftCertificate(String name, String description, float price, int duration, Set<Tag> tags) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.createDate = ZonedDateTime.now(ZoneOffset.UTC);
        this.lastUpdateDate = createDate;
        this.tags = tags;
    }

    public void deleteTag(Tag tag) {
        tags.remove(tag);
    }

    public void addTag(Tag tag) {
        tags.add(tag);
    }

    public void buildLinks() {
        Link selfLink = linkTo(CertificateController.class).slash(id).withSelfRel();
        if (!hasLink("self")) {
            add(selfLink);
        }
        if (getTags() != null) {
            getTags().forEach(t -> t.buildLinks());
        }
    }
}
