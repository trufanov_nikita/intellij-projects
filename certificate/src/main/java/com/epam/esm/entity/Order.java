package com.epam.esm.entity;

import com.epam.esm.audit.AuditableAndRepresentation;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;

import java.time.ZonedDateTime;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.TimeZoneStorage;
import org.hibernate.annotations.TimeZoneStorageType;

@Entity
@Table(name = "user_certificate")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Order extends AuditableAndRepresentation<String> {
    @EmbeddedId
    private OrderKey id;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @MapsId("certificateId")
    @JoinColumn(name = "certificate_id")
    private GiftCertificate certificate;

    private float price;

    @Column(name = "purchase_date")
    @TimeZoneStorage(TimeZoneStorageType.NATIVE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    private ZonedDateTime purchaseDate;

    public void buildLinks() {
        if (this.certificate != null) {
            certificate.buildLinks();
        }
        if (this.user != null) {
            user.buildLinks();
        }
    }
}
