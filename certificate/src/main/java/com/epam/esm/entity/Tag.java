package com.epam.esm.entity;

import com.epam.esm.audit.AuditableAndRepresentation;
import com.epam.esm.controller.TagController;
import jakarta.annotation.Nonnull;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Link;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@Entity
@Table(name = "tag")
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
public class Tag extends AuditableAndRepresentation<String> {
    @Id
    @GeneratedValue
    private Long id;

    @Nonnull
    private String name;


    public Tag() {
        this.name = "default";
    }

    public Tag(String name) {
        this.name = name;
    }

    public void buildLinks() {
        Link tagLink = linkTo(methodOn(TagController.class).getById(id)).withSelfRel();
        if (!this.hasLink("self")) {
            this.add(tagLink);
        }
    }
}
