package com.epam.esm.audit;

import com.epam.esm.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

@Log4j2
public class AuditorAwareImpl implements AuditorAware<String> {
    @Autowired
    private UserRepository userRepository;

    @Override
    public Optional<String> getCurrentAuditor() {
        String user = SecurityContextHolder.getContext().getAuthentication().getName();
        log.info("Auditor: " + user);
        return Optional.ofNullable(user);
    }
}