'use strict';

console.log(secondsToDate(31536000));
console.log(toBase2Converter(11));
console.log(repeatingLitters("Hello world"));
console.log(substringOccurrencesCounter('a', "aaa thrre "));


console.log(gather("a")("b")("c").order(0, 1, 2).get()); // "abc"
console.log(gather("a")("b")("c").order(2, 1, 0).get()); // "cba"
console.log(gather("e")("l")("o")("l")("!")("h").order(5, 0, 1, 3, 2, 4).get()); // "hello"

/**
 * You must return a date that comes in a predetermined number of seconds after 01.06.2020 00:00:002020
 * @param {number} seconds
 * @returns {Date}
 *
 * @example
 *      31536000 -> 01.06.2021
 *      0 -> 01.06.2020
 *      86400 -> 02.06.2020
 */
function secondsToDate(seconds) {
    var date = new Date("2020-06-01T00:00:00.2020Z");
    date.setSeconds(date.getSeconds() + seconds);
    return date;
}

/**
 * You must create a function that returns a base 2 (binary) representation of a base 10 (decimal) string number
 * ! Numbers will always be below 1024 (not including 1024)
 * ! You are not able to use parseInt
 * @param {number} decimal
 * @return {string}
 *
 * @example
 *      5 -> "101"
 *      10 -> "1010"
 */
function toBase2Converter(decimal) {
    return decimal.toString(2);
}

/**
 * You must create a function that takes two strings as arguments and returns the number of times the first string
 * is found in the text.
 * @param {string} substring
 * @param {string} text
 * @return {number}
 *
 * @example
 *      'a', 'test it' -> 0
 *      't', 'test it' -> 2
 *      'T', 'test it' -> 2
 */
function substringOccurrencesCounter(substring, text) {
    const regex = new RegExp(substring, "gi");
    return (text.match(regex) || []).length;
}

/**
 * You must create a function that takes a string and returns a string in which each character is repeated once.
 *
 * @param {string} string
 * @return {string}
 *
 * @example
 *      "Hello" -> "HHeelloo"
 *      "Hello world" -> "HHeello  wworrldd" // o, l is repeated more then once. Space was also repeated
 */
function repeatingLitters(string) {
    const result = [];
    for (const letter of string) {
        if(substringOccurrencesCounter(letter, string) < 2){
            result.push(letter, letter);
        }
        else{
            result.push(letter);
        }
    }
    return result.join('');
  }

/**
 * You must write a function redundant that takes in a string str and returns a function that returns str.
 * ! Your function should return a function, not a string.
 *
 * @param {string} str
 * @return {function}
 *
 * @example
 *      const f1 = redundant("apple")
 *      f1() ➞ "apple"
 *
 *      const f2 = redundant("pear")
 *      f2() ➞ "pear"
 *
 *      const f3 = redundant("")
 *      f3() ➞ ""
 */
function redundant(str) {
    return function () {
        return str;
      };
}

/**
 * https://en.wikipedia.org/wiki/Tower_of_Hanoi
 *
 * @param {number} disks
 * @return {number}
 */
function towerHanoi(disks) {
    if (disks === 0) {
        return 0;
      }
      
    return 2 * towerHanoi(disks - 1) + 1;
}

/**
 * You must create a function that multiplies two matricies (n x n each).
 *
 * @param {array} matrix1
 * @param {array} matrix2
 * @return {array}
 *
 */
function matrixMultiplication(matrix1, matrix2) {
    const n = matrix1.length;
    const result = [];
  
    for (let i = 0; i < n; i++) {
      result[i] = [];
      for (let j = 0; j < n; j++) {
        let sum = 0;
        for (let k = 0; k < n; k++) {
          sum += matrix1[i][k] * matrix2[k][j];
        }
        result[i][j] = sum;
      }
    }
  
    return result;
  }

/**
 * Create a gather function that accepts a string argument and returns another function.
 * The function calls should support continued chaining until order is called.
 * order should accept a number as an argument and return another function.
 * The function calls should support continued chaining until get is called.
 * get should return all of the arguments provided to the gather functions as a string in the order specified in the order functions.
 *
 * @param {string} str
 * @return {string}
 *
 * @example
 *      gather("a")("b")("c").order(0)(1)(2).get() ➞ "abc"
 *      gather("a")("b")("c").order(2)(1)(0).get() ➞ "cba"
 *      gather("e")("l")("o")("l")("!")("h").order(5)(0)(1)(3)(2)(4).get()  ➞ "hello"
 */
function gather(str) {
    let args = [str];
    
    function inner(arg) {
      args.push(arg);
      return inner;
    }
    
    inner.order = function() {
      let orderArgs = Array.from(arguments);
      
      function getOrder() {
        let orderedArgs = orderArgs.map(index => args[index]);
        return orderedArgs.join("");
      }
      
      getOrder.order = function() {
        let newOrderArgs = Array.from(arguments);
        orderArgs = newOrderArgs;
        return getOrder;
      };
      
      getOrder.get = function() {
        return getOrder();
      };
      
      return getOrder;
    };
    
    return inner;
  }