let loading = false;
let typingTimer;
// Standart categories
const categories = ["Car", "Sport", "Tourism", "Flight", "Photo"];
const searchInput = document.getElementById('searchInput');
const selectInput = document.getElementById('selectInput');
const categoryWrapper = document.querySelector('.category-wrapper');
const doneTypingInterval = 500;
const debouncedScrollHandler = _.debounce(scrollHandler, 500);
const categoryElements = document.querySelectorAll('.category');
let maxScrollPosition = 0;
const scrollThreshold = 300;
// Prepared images for infinite scroll
const couponImages = [
  'https://img.freepik.com/premium-photo/raster-illustration-river-lavender-field-beautiful-scenery-landscape-view-purple-tones-purple-pink-clouds-flowers-plants-wildlife-beauty-nature-concept-3d-artwork-rendering_76964-889.jpg?w=2000',
  'https://cdn.wallpapersafari.com/23/43/8phWXR.jpg',
  'https://i.ytimg.com/vi/u71QsZvObHs/maxresdefault.jpg',
  'https://m.media-amazon.com/images/I/81YuPZAcFhS.jpg',
  'https://images.unsplash.com/photo-1605125950879-a81fe58d8439?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8YmVhdXRpZnVsJTIwc2NlbmVyeXxlbnwwfHwwfHx8MA%3D%3D&w=1000&q=80'
];

window.addEventListener('scroll', debouncedScrollHandler);

categories.forEach(category => {
  const categoryElement = document.createElement('div');
  categoryElement.classList.add('category');
  categoryElement.innerHTML = `
        <img src="../assets/category-image-${category}.jpg" alt="category image" class="category-image">
        <span class="category-text">${category}</span>
    `;
  categoryWrapper.appendChild(categoryElement);
});

// Place category options in select element
categories.forEach(category => {
  const option = document.createElement('option');
  option.value = category;
  option.textContent = category;
  selectInput.appendChild(option);
});

categoryElements.forEach(category => {
  category.addEventListener('click', () => {
    const categoryName = category.querySelector('.category-text').textContent;
    performSearch("", categoryName);
  });
});

searchInput.addEventListener('input', () => {
  console.log("Search");
  clearTimeout(typingTimer);
  typingTimer = setTimeout(() => {
    const searchTerm = searchInput.value;
    const selectedCategory = selectInput.value;
    console.log(selectedCategory)
    performSearch(searchTerm, selectedCategory);
  }, doneTypingInterval);
});

selectInput.addEventListener('change', () => {
  console.log("Search");
  clearTimeout(typingTimer);
  typingTimer = setTimeout(() => {
    const searchTerm = searchInput.value;
    const selectedCategory = selectInput.value;
    console.log(selectedCategory)
    performSearch(searchTerm, selectedCategory);
  }, doneTypingInterval);
});

function performSearch(searchTerm, selectedCategory) {
  const coupons = Array.from(document.querySelectorAll('.coupon'));

  const filteredCoupons = coupons.filter(coupon => {
    const couponName = coupon.querySelector('.coupon-name-text').textContent;
    const couponCategory = coupon.querySelector('.coupon-category').textContent;
    const showCoupon = matchSearchTerm(searchTerm, couponName) && matchCategory(selectedCategory, couponCategory);

    coupon.style.display = showCoupon ? 'block' : 'none';
    return showCoupon;
  });

  const sortedCoupons = filteredCoupons.sort((a, b) => {
    const dateA = new Date(a.querySelector('.coupon-expire').dateTime);
    const dateB = new Date(b.querySelector('.coupon-expire').dateTime);
    return dateA - dateB;
  });

  const couponWrapper = document.querySelector('.coupon-wrapper');
  sortedCoupons.forEach(coupon => {
    couponWrapper.appendChild(coupon);
  });
}

function matchSearchTerm(searchTerm, couponName) {
  return couponName.toLowerCase().includes(searchTerm.toLowerCase());
}

function matchCategory(selectedCategory, couponCategory) {
  return selectedCategory.toLowerCase() === 'all categories' || selectedCategory.toLowerCase() === couponCategory.toLowerCase();
}

function scrollHandler() {
  const currentScrollPosition = window.scrollY;
  const maxScrollPositionReached = currentScrollPosition >= maxScrollPosition;
  if (maxScrollPositionReached && currentScrollPosition + window.innerHeight >= document.documentElement.scrollHeight - scrollThreshold) {
    if (!loading) {
      showLoading();
      loading = true;
      const newCoupons = generateCoupons(5);
      setTimeout(() => {
        appendCoupons(newCoupons);
        maxScrollPosition = currentScrollPosition;
        hideLoading();
        loading = false;
      }, 1500);
    }
  }
}

function showLoading() {
  const footer = document.querySelector('.footer');
  const loadingElement = document.createElement('div');
  loadingElement.classList.add('loading');
  loadingElement.innerHTML = '<img src="https://icon-library.com/images/loading-icon-transparent-background/loading-icon-transparent-background-12.jpg" alt="Loading..." class="loading-gif">';
  footer.appendChild(loadingElement);
}

function hideLoading() {
  const footer = document.querySelector('.footer');
  const loadingElement = footer.querySelector('.loading');
  if (loadingElement) {
    footer.removeChild(loadingElement);
  }
}

const intersectionCallback = (entries, observer) => {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      const img = entry.target;
      const src = img.getAttribute('data-src');
      img.setAttribute('src', src);
      observer.unobserve(img);
    }
  });
};

function getRandomImage() {
  const randomIndex = Math.floor(Math.random() * couponImages.length);
  return couponImages[randomIndex];
}

function appendCoupons(coupons) {
  const couponWrapper = document.querySelector('.coupon-wrapper');

  coupons.forEach(coupon => {
    const couponElement = document.createElement('div');
    couponElement.classList.add('coupon');
    couponElement.innerHTML = `
        <div class="coupon-image-wrapper">
            <img data-src="${getRandomImage()}" alt="coupon-image" class="coupon-image">
        </div>
        <div class="coupon-name-wrapper">
          <span class="coupon-name-text">${coupon.name}</span>
          <img src="../assets/like.svg" alt="like" class="like-image-coupon">
        </div>
        <div class="coupon-name-wrapper">
          <span class="coupon-category">${coupon.category}</span>
        </div>
        <div class="coupon-desc-wrapper">
          <span class="coupon-desc">${coupon.description}</span>
          <time datetime="${coupon.expireDate}" class="coupon-expire">${coupon.expireDate}</time>
        </div>
        <div class="coupon-buy-wrapper">
          <span>${coupon.price}</span>
          <button type="submit" class="add-to-cart">Add to Cart</button>
        </div>
      `;

    couponWrapper.appendChild(couponElement);
    const observer = new IntersectionObserver(intersectionCallback, { rootMargin: '50px' });
    // Observe coupon image for lazy loading

    const img = couponElement.querySelector('.coupon-image');
    img.setAttribute('src', '../assets/temp-coupon-image.jpg');
    img.onload = () => {
      img.setAttribute('src', img.getAttribute('data-src'));

    };
    img.onerror = () => {
      img.setAttribute('src', '../assets/temp-coupon-image.jpg'); // Replace with your placeholder image path

      observer.unobserve(img); // Stop observing the image
    };
    observer.observe(img);

    // Set the image source after attaching the observer
    img.setAttribute('src', img.getAttribute('data-src'));

    // if some links is broken its the only way to stop browser trying to load image
    //   const img = couponElement.querySelector('.coupon-image');
    //   fetch(img.getAttribute('data-src'))
    //   .then(response => {
    //     if (!response.ok) {
    //       throw new Error('Image load error');
    //     }
    //     return response.blob();
    //   })
    //   .then(blob => {
    //     const url = URL.createObjectURL(blob);
    //     img.setAttribute('src', url);
    //   })
    //   .catch(error => {
    //     img.setAttribute('src', '../assets/temp-coupon-image.jpg');
    //     img.removeAttribute('data-src');
    //     observer.unobserve(img);
    //   });
    // observer.observe(img);
    // });
  });
}

function generateCoupons(count) {
  const newCoupons = [];

  const currentDate = new Date();
  const futureDate = new Date("2030-12-31");

  for (let i = 0; i < count; i++) {
    const randomIndex = Math.floor(Math.random() * categories.length);
    const randomCategory = categories[randomIndex];

    const expireDate = getRandomDate(currentDate, futureDate);

    const coupon = {
      name: `Coupon ${i + 1}`,
      category: randomCategory,
      description: "Some brief description",
      expireDate: expireDate.toISOString().split("T")[0],
      price: "$100"
    };

    newCoupons.push(coupon);
  }

  return newCoupons;
}

function getRandomDate(start, end) {
  const startDate = start.getTime();
  const endDate = end.getTime();
  const randomDate = new Date(startDate + Math.random() * (endDate - startDate));
  return randomDate;
}

window.addEventListener('DOMContentLoaded', () => {
  const button = document.getElementById('scrollButton');
  button.addEventListener('click', () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  });
});

window.addEventListener('load', () => {
  const loadingOverlay = document.querySelector('.loading-overlay');
  loadingOverlay.style.display = 'none';
});

